import axios from 'axios';
import { API } from '../configs'

export default () => {
    async function updateCase (req) {
        let returnJSON = null;
        let case_id = req.case_id;
        let connectAPI = API.UPDATE_CASE+'?id='+case_id;
        let body = {
            firstname : req.firstname,
            lastname: req.lastname,
            status: req.status,
            Languages : req.Languages,
            age : req.age
        }
        await axios.put(connectAPI,body).then((res)=>{
            const { status , response } = res ;
            returnJSON = response;
        })
        return returnJSON;
    }

    return [updateCase]
}