import axios from 'axios';
import { API } from '../configs'

export default () => { 
    async function createCPR (req) {
        console.log('body : ',req)
        let responseJSON = null;
        let body = {
            userID : req.userID,
            caseID: req.caseID,
            medicine: req.medicine,
            EKG : req.EKG,
            Defib: req.Defib,
            status: req.status,
            Round: req.Round,
            Languages: 'TH'
        }
        console.log(API.CREATE_CPR)
        await axios.post(API.CREATE_CPR,body).then((res)=>{
            const {status ,data} = res;
            console.log('COMPLETE : status ',status)
            responseJSON = data;
        })
    }

    return [createCPR]
}