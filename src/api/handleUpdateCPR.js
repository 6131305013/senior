import axios from 'axios';
import { API } from '../configs'

export default () => {
    async function updateCPR (req) {
        let returnJSON = null;
        let case_id = req.case_id;
        let round = req.round;
        let connectAPI = API.UPDATE_CPR+'?id='+case_id+'&round='+round;
        let body = {
            medicine : req.medicine,
            EKG: req.EKG,
            Defib: req.Defib
        }
        await axios.put(connectAPI,body).then((res)=>{
            const { status , response } = res ;
            returnJSON = response;
        })
        return returnJSON;
    }

    return [updateCPR]
}