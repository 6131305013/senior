import axios from 'axios';
import { API } from '../configs'

export default () => { 
    async function getCompleteLesson (ID) {
        let returnJson = null;
        let connectAPI = API.GET_LESSON_COMPLETE+'?userID='+ID
        await axios.get(connectAPI).then((res)=>{
            let {status,data} = res;
            returnJson = data.data
        }).catch((err)=>{
            console.log('ERROR : ',err)
        })
        return returnJson
    }

    return [getCompleteLesson]
}