import axios from 'axios';
import { API } from '../configs'

export default () => {
    async function GetMedicineByType(req){
        let returnJSON = null ;
        let body = {
            type : req.type,
            Age : req.Age,
            Unit : req.Unit
        };
        await axios.post(API.GET_MEDICINE_BY_TYPE,body).then((res)=>{
            const { status , response } = res ;
            returnJSON = response;
        })
        return returnJSON;
    }
    return [GetMedicineByType]
 }