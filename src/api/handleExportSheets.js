import axios from 'axios';
import { API } from '../configs'

export default () => {

    async function ExportSheets(req){
        let returnJSON = null ;
        let body = {
            mail : req.mail,
            uid : req.userId,
            cid : req.caseId
        };
        await axios.post(API.EXPORT_SHEETS,body).then((res)=>{
            const { status , response } = res ;
            returnJSON = response;
        })
        return returnJSON;
    }
    return [ExportSheets]
 }