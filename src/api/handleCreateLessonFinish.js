import axios from 'axios';
import { API } from '../configs'

export default () => { 
    async function completeLesson (req) {
        let returnJson = null;
        let body = {
            userID : req.userID,
            type : req.type,
            complete : req.complete,
        }
        await axios.post(API.CREATE_LEARNING_COMPLETE,body).then((res)=>{
            let {status,data} = res;
            returnJson = data.data
        }).catch((err)=>{
            console.log('ERROR : ',err)
        })
        return returnJson
    }

    return [completeLesson]
}