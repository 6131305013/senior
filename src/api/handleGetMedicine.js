import axios from 'axios';
import { API } from '../configs'

export default () => {
    async function getMedicine() {
        let returnJSON = null;
        // console.log(1)
        await axios.get(API.GET_MEDICINE).then((res) => {
            // console.log(2)
            const { status, data } = res;
            // console.log(data)
            returnJSON = data.data;
        })

        return returnJSON;
    }

    return [getMedicine]
}