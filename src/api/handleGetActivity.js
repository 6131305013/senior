import axios from 'axios';
import { API } from '../configs'

export default () => { 
    async function getAllActivity (req) {
        console.log('body : ',req)
        let responseJSON = null;
        console.log(API.CREATE_CPR)
        await axios.get(API.ALL_ACTIVITY).then((res)=>{
            const {status ,data} = res;
            console.log('COMPLETE : status ',status)
            responseJSON = data;
        })
        return responseJSON;
    }

    async function getActivityByUIDandCID (uid,cid){
        let responseJSON = null;
        let connectAPI = API.ACTIVITY_BY_CASE_UID+'?uid='+uid+'&cid='+cid
        // console.log(connectAPI)
        await axios.get(connectAPI).then((res)=>{
            const {status ,data} = res;
            console.log('COMPLETE : status ',status)
            // console.log('COMPLETE : data ',data)
            responseJSON = data;
        })
        return responseJSON;
    }

    return [getAllActivity,getActivityByUIDandCID]
}