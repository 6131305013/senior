import axios from 'axios';
import { API } from '../configs'

export default () => {

   async function getAllcase() {
      let returnJSON = null;
      await axios.get(API.GET_ALL_CASE).then((res) => {
         const { status, data } = res;
         returnJSON = data.data;
      })
      return returnJSON;
   }

   async function getCaseByID(id) {
      let returnJSON = null;
      let connectAPI = API.GET_CASE_BY_ID + '?id=' + id;
      await axios.get(connectAPI).then((res) => {
         const { status, data } = res;
         returnJSON = data.data;
      })
      return returnJSON;
   }

   async function getCaseByUserId(id) {
      let returnJSON = null;
      let connectAPI = API.GET_CASE_BY_UID + '?id=' + id;
      console.log("API ===> ", connectAPI)
      await axios.get(connectAPI).then((res) => {
         const { status, data } = res;
         console.log("user", data.data)
         returnJSON = data.data;
      })
      return returnJSON;
   }

   return [getAllcase, getCaseByID, getCaseByUserId]
}