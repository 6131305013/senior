import axios from 'axios';
import { API } from '../configs'

export default () => {
    async function getLesson(type) {
        let returnJSON = null;
        // console.log(1)
        let connectAPI = API.GET_LEARNING+'?type='+type
        console.log('connectAPI ',connectAPI)
        await axios.get(connectAPI).then((res) => {
            // console.log(2)
            const { status, data } = res;
            console.log(data)
            returnJSON = data.data;
        })

        return returnJSON;
    }

    return [getLesson]
}