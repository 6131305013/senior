import axios from 'axios';
import { API } from '../configs'

export default () => {
    async function getPratice(type) {
        let returnJSON = null;
        // console.log(1)
        let connectAPI = API.GET_PRACTICE+'?type='+type
        await axios.get(connectAPI).then((res) => {
            // console.log(2)
            const { status, data } = res;
            // console.log(data)
            returnJSON = data.data;
        })

        return returnJSON;
    }

    return [getPratice]
}