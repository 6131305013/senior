import axios from 'axios';
import { API } from '../configs'

export default () => {
    async function createCase(req) {
        let responseJSON = null;
        let body = {
            userId: req.userId,
            firstname: req.firstname,
            lastname: req.lastname,
            status: req.status,
            age: req.age,
            Languages: req.Languages
        }
        console.log('createCase : ',body)
        await axios.post(API.CREATE_CASE, body).then((res) => {
            const { status, data } = res;
            responseJSON = data;
        })
        return responseJSON;
    }

    return [createCase]
}