import axios from 'axios';
import { API } from '../configs'

export default () => { 
    async function CompletePractice (req) {
        let returnJson = null;
        let body = {
            userID : req.userID,
            type : req.type,
            score : req.score,
        }
        await axios.post(API.CREATE_PRACTICE_COMPLETE,body).then((res)=>{
            let {status,data} = res;
            returnJson = data.data
        }).catch((err)=>{
            console.log('ERROR : ',err)
        })
        return returnJson
    }

    return [CompletePractice]
}