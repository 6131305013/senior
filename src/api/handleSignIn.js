import { useState, useContext } from 'react';
import qs from 'qs';
import axios from 'axios';
import { API } from '../configs';
import AsyncStorage from '@react-native-community/async-storage';
import { Context as AuthContext } from '../controllers/AuthController';
export default () => {
    const { state, getAuth } = useContext(AuthContext);
    let now = new Date();
    let expireTime = new Date(now);
    async function SignIn(username, password) {

        var bodyShow = qs.stringify({
            username: username,
            password: password,
        });
        var Body = {
            username: username,
            password: password,
        }

        if ((await AsyncStorage.getItem('ALERT_SIGNIN')) === 'true') {
            alert(bodyShow);
        }

        await axios.post(API.SIGN_IN, Body).then(async (res) => {
            const { status, data } = res;
            console.log('[ ✅ ] SUCCESS_CREATE_TOKEN : ', status);
            var userExpireTime = expireTime.setSeconds(
                now.getSeconds() + res.data.expires_in,
            );
            const userToken = JSON.stringify(res.data);
            console.log(userToken);
            await AsyncStorage.setItem(API.AS_USER_TOKEN, userToken);
            await AsyncStorage.setItem(
                API.AS_USER_EXPIRY,
                userExpireTime.toString(),
            );

            console.log('START SET AUTHEN !');
            await getAuth();
        }).catch((err) => {
            const { status, data } = err.response;
            // TODO: Create catch function follow status code
            console.log('[ ❌ ] ERROR_LOGIN_STATUS : ', status);
            console.log('[ ❌ ] ERROR_LOGIN_DATA : ', data);
            
          });
    }

    return [SignIn]
}