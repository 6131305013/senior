import axios from 'axios';
import { API } from '../configs'

export default () => {
    async function deleteCase (req) {
        let returnJSON = null;
        let case_id = req;
        let connectAPI = API.DELETE_CASE+'?id='+case_id;
        await axios.put(connectAPI).then((res)=>{
            const { status , response } = res ;
            returnJSON = response;
        })
        return returnJSON;
    }

    return [deleteCase]
}