import axios from 'axios';
import { API } from '../configs'

export default () => {

    async function createActivity(req){
        let returnJSON = null ;
        let body = {
            activity : req.activity,
            name : req.name,
            userId : req.userId,
            caseId : req.caseId
        };
        await axios.post(API.CREATE_ACTIVITY,body).then((res)=>{
            const { status , response } = res ;
            returnJSON = response;
        })
        return returnJSON;
    }
    return [createActivity]
 }