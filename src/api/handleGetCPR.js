import axios from 'axios';
import { API } from '../configs'

export default () => {

    async function getAllcpr() {
        let returnJSON = null;
        await axios.get(API.GET_ALL_CPR).then((res) => {
            const { status, data } = res;
            returnJSON = data.data;
        })
        return returnJSON;
    }

    async function getCprByID(id,userId) {
        let returnJSON = null;
        let connectAPI = API.GET_CPR_BY_CASE + '?CaseId=' + id + '&UserId='+userId;
        console.log('getCprByID : ',connectAPI)
        await axios.get(connectAPI).then((res) => {

            const { status, data } = res;
            returnJSON = data.data;
        })
        return returnJSON;
    }

    return [getAllcpr, getCprByID]
}