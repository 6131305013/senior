import axios from 'axios';
import { API } from '../configs'

export default () => {
    async function deleteCPR(id) {
        let returnJSON = null;
        let case_id = id;
        let connectAPI = API.DELETE_CPR+'?id='+case_id;
        await axios.put(connectAPI).then((res)=>{
            const { status , response } = res ;
            returnJSON = response;
        })
        return returnJSON;
    }

    return [deleteCPR]
}