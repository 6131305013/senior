import React, { useState, useEffect } from 'react'
import { FlatList, Dimensions } from 'react-native';
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableHighlight,
} from 'react-native';
import { SelectionModal } from '../components';
import { scale, verticalScale, moderateScale } from '../configs/scale';
import { COLORS, SIZES, FONTS } from '../themes';
import { Res } from '../configs';


const SelectLearnOrPractice = (props) => {
    const { navigation } = props;
    return (
        <View style={styles.container}>
            <SelectionModal
                title={'Learning'}
                titleTextColor={COLORS.rosc}
                titleModalColor={COLORS.list}
                titleImage={Res.Image.Books}
                choice={[
                    {
                        name: 'Child',
                        onPress: () => navigation.navigate('Learning', { type: 'children' }),
                        color: COLORS.child,
                    },
                    {
                        name: 'Adult',
                        onPress: () => navigation.navigate('Learning', { type: 'adult' }),
                        color: COLORS.adult
                    },
                    // {
                    //     name: 'Patient',
                    //     onPress: () => navigation.navigate('Learning',{type:''}),
                    //     color: 'blue'
                    // }
                ]}
            />
            <SelectionModal
                title={'Practice'}
                titleTextColor={COLORS.secondary}
                titleModalColor={COLORS.lightblue}
                titleImage={Res.Image.practice}
                titlePosition={'right'}
                choice={[
                    {
                        name: 'Child',
                        onPress: () => navigation.navigate('Practice', { type: 'children' }),
                        color: COLORS.child,
                    },
                    {
                        name: 'Adult',
                        onPress: () => navigation.navigate('Practice', { type: 'adult' }),
                        color: COLORS.adult
                    },
                    // {
                    //     name: 'Patient',
                    //     onPress: () => navigation.navigate('Practice',{type:''}),
                    //     color: 'blue'
                    // }
                ]}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    elevation: {
        elevation: 20,
        shadowColor: '#52006A',
    },
})
export default SelectLearnOrPractice;