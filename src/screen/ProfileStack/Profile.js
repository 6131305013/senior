import React, { useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableHighlight,
} from 'react-native';

const Profile = (props) => {
    const {navigation} = props

    return (
        <View style={styles.container}>
            <Text>Profile Screen</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container : {
        flex:1,
        alignContent:'center',
        alignItems:'center',
        alignSelf:'center'
    }
})
export default Profile;