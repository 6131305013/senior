import React, { useState, useEffect } from 'react'
import { FlatList, Dimensions } from 'react-native';
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableHighlight,
} from 'react-native';
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import handleGetLesson from '../../api/handleGetLesson'
import { scale, verticalScale, moderateScale } from '../../configs/scale';

const { width } = Dimensions.get('window');

const Learning = (props) => {
    const { navigation } = props
    const { type } = props.route.params;
    const [getLesson] = handleGetLesson();
    const [step, setStep] = useState([
        {
            title: 'Hello Swiper',
            subtitle: 'test',
            step: 1
        }, {
            title: 'Beautiful',
            subtitle: 'test',
            step: 2
        }, {
            title: 'And simple',
            subtitle: 'test',
            step: 3
        }
    ])

    useEffect(() => {
        loadData();
    }, [])


    const loadData = async () => {
        let getData = await getLesson(type);
        let Arrays = [];
        for (let i in getData) {
            let Object = {
                title: getData[i].title,
                subtitle: getData[i].subtitle,
                step: getData[i].step
            }
            Arrays.push(Object)
        }
        console.log('Arrays', Arrays)
        setStep(Arrays)
    }

    const _renderPage = ({ item }) => {
        console.log('ITEM : ', item)
        return (
            <View>

                <View style={[styles.child, { backgroundColor: '#FFFFFF', }]}>
                    <Image
                        style={styles.tinyLogo}
                        source={{
                            uri: 'https://cpr.heart.org/-/media/cpr-images/resources/what-is-cpr/cpr-072204-body.jpg?h=479&la=en&mw=600&w=600&hash=8C11D0025C613FCC9D412404B52A96AC34619A52',
                        }}
                    />

                    <View style={styles.cardDetail}>
                        <Text style={[styles.textDetail, { textAlign: 'center' }]}>{item.title}</Text>
                        {item.subtitle != 'null' && <Text style={styles.textDetail}>{item.subtitle}</Text>}
                    </View>
                </View>

                <View style={styles.circleStep}>
                    <Text style={styles.textStep}> ขั้นตอนที่ </Text>
                    <Text style={styles.textStep}>{item.step}</Text>
                </View>

            </View>

        )
    }

    return (
        <View style={styles.container}>
            {/* <Text>Learning Screen</Text> */}
            <View>
                <TouchableHighlight onPress={() => navigation.navigate('Practice')}>
                    <Text>Go to Practice Screen</Text>
                </TouchableHighlight>
            </View>
            <SwiperFlatList
                index={0}
                showPagination
                data={step}
                renderItem={_renderPage}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    child: {
        width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStep: {
        fontSize: 18,
        color: 'black',
        fontWeight: '700'
    },
    textDetail: {
        fontSize: 21,
        color: 'black',
        paddingHorizontal: scale(5),
        fontWeight: 'bold'
    },
    tinyLogo: {
        width: '80%',
        height: '50%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    circleStep: {
        height: 100,
        width: 100,
        borderRadius: moderateScale(50),
        backgroundColor: "#FFFFFF",
        elevation: 3,
        justifyContent: 'center',
        alignItems: 'center',
        top: "-50%",
        marginHorizontal: scale(30),
    },
    cardDetail: {
        height: "45%",
        width: "70%",
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        borderBottomLeftRadius: moderateScale(15),
        borderBottomRightRadius: moderateScale(15),
        elevation: 3,
        justifyContent: 'center'
    }
})
export default Learning;