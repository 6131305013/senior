import React, { useEffect, useState, useCallback } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableHighlight,
    Button,
    FlatList,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import { Modal } from '../../components'
import AnimatedCircularProgress from 'react-native-animated-circular-progress';
import { progressColor, progressPercent } from '../../functions'
import moment from 'moment';
import { Res } from '../../configs';
import mockup from '../../configs/mockup'
import EvilIcons from "react-native-vector-icons/EvilIcons";
import LottieView from 'lottie-react-native';
import handleGetCompleteLesson from '../../api/handleGetCompleteLesson';
import handleGetCompletePractice from '../../api/handleGetCompletePractice';
import CustomHeader from '../../navigation/CustomHeader';
import { DetailByType } from '../../functions/main';
const numColumns = 2

const HomeScreen = (props) => {
    const { navigation } = props;
    const { user } = mockup;
    const [Visible, setVisible] = useState(false);
    const [HistoryTypeState, setHistoryTypeState] = useState(null);
    const [HistoryPersonType, setHistoryPersonType] = useState(null);
    const [HistoryDateState, setHistoryDateState] = useState(null);
    const [HistoryscoreState, setHistoryscoreState] = useState(null);
    const [isData, setIsData] = useState(mockup.STAT)
    const [isSelect, setIsSelect] = useState('Practice')
    const [getCompleteLesson] = handleGetCompleteLesson();
    const [getCompletePractice] = handleGetCompletePractice();

    useEffect(() => {
        setDatainState()
    }, [isSelect])

    const SelectedType = (type) => {
        console.log('type : ', type)
        setIsSelect(type)
        setDatainState()
    }

    const setDatainState = async () => {
        let Data = await DetailByType(user.id,isSelect);
        setIsData(Data)
    }
    const SwitchVisible = () => {
        setVisible(!Visible);
    }

    const SelectItemtoShow = async (item) => {
        setHistoryDateState(item.Date)
        setHistoryPersonType(item.typePerson)
        setHistoryTypeState(item.type)
        setHistoryscoreState(item.score)
    }

    const _renderItem = ({ item }) => {

        return (
            <View style={styles.cards}>
                <View style={{ justifyContent: 'space-between', marginTop: 15, marginButtom: 15 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View>
                            <Text style={{ fontSize: 25, fontWeight: 'bold', marginLeft: 30 }}>{item.title}</Text>
                            <View style={{ height: 50, width: 200 }}>
                                <Text style={{ fontSize: 18, marginLeft: 30 }}>{item.subtitle}</Text>
                            </View>
                        </View>

                        <Image
                            source={require('../../images/heart-rate.png')}
                            style={{ width: 150, height: 150, marginTop: 25 }}
                        />
                    </View>

                    <TouchableOpacity onPress={() => {
                        navigation.navigate('Screen2')
                    }}>
                        <View style={styles.circleGO}>
                            <Text style={{ marginLeft: 5, marginBottom: 5 }}>GO</Text>
                        </View>

                    </TouchableOpacity>
                </View>


            </View>
        )
    };

    const _renderHistoryItem = ({ item }) => {
        return (
            <TouchableOpacity
                disabled={item.type == 'Practice' ? false : true}
                onPress={async () => {
                    // console.log('click ==> Data : ',item)
                    await SelectItemtoShow(item).then(() => {
                        SwitchVisible();
                    })
                }}>
                <View style={styles.cardStat}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={[styles.ModalCenter]}>
                            {item.type == 'Practice' ?
                                <View>
                                    <AnimatedCircularProgress
                                        backgroundColor='#eeeeee'
                                        innerBackgroundColor='white'
                                        color={progressColor(item.score)}
                                        startDeg={0}
                                        endDeg={progressPercent(item.score)}
                                        radius={70}
                                        innerRadius={60}
                                        duration={1000}
                                        children={(
                                            <View style={{ alignItems: 'center', marginTop: 35 }}>
                                                <Text style={{ fontSize: 35, color: progressColor(item.score) }}>{parseInt(item.score)}%</Text>
                                            </View>
                                        )}
                                    />
                                </View> :
                                <View>
                                    {
                                        item.success == true ?
                                            <LottieView source={Res.Animation.correct} autoPlay loop={false} style={[{ width: 100, height: 100 }]} />
                                            :
                                            <LottieView source={Res.Animation.failed} autoPlay loop={false} style={[{ width: 80, height: 80 }]} />
                                    }

                                </View>
                            }

                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text style={styles.textTypePerson}>{item.typePerson}</Text>
                            <Text style={styles.titleStat(item.success)}>{item.title}</Text>
                            <Text style={styles.textDate}>{moment(item.Date).format('DD MMM YYYY')} </Text>
                        </View>
                    </View>

                    <Text style={{ textAlign: "center", fontSize: 12, color: '#152383' }}>{item.type == 'Practice' ? 'View detail' : 'Learn agian'}</Text>

                </View>
            </TouchableOpacity>
        )
    }

    const _renderSuggestion = () => {
        return (
            <View>
                <ScrollView>
                    <View>
                        <FlatList
                            horizontal
                            data={mockup.DATA}
                            renderItem={_renderItem}
                            keyExtractor={item => `${item.id}`}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }

    const _renderHistory = () => {
        return (
            <View>
                <View >
                    <Text style={styles.HistoryHeaderText}>History</Text>
                </View>

                <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal:"18%" }}>
                        <TouchableOpacity
                            onPress={() => { SelectedType('Practice') }}>
                            <Text style={[styles.HistoryTitle, { color: isSelect == 'Practice' ? 'green' : 'black', textDecorationLine: isSelect == 'Practice' ? 'underline' : 'none' }]}>Practice </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => { SelectedType('Learning') }}>
                            <Text style={[styles.HistoryTitleLearn, { color: isSelect == 'Learning' ? 'green' : 'black', textDecorationLine: isSelect == 'Learning' ? 'underline' : 'none' }]}>Learning</Text>
                        </TouchableOpacity>

                    </View>
                    <ScrollView>
                        <View style={{ height: 670, marginTop: 15 }} >
                            <FlatList
                                data={isData}
                                renderItem={_renderHistoryItem}
                                keyExtractor={item => `${item.id}`}
                                // horizontal={true}
                                columnWrapperStyle={styles.Wrapper}
                                numColumns={numColumns}
                            />
                        </View>
                    </ScrollView>

                </View>
            </View >
        )
    }


    return (
        <View style={styles.container}>
            <CustomHeader title={'WELCOME'}/>
            <Modal
                modalVisible={Visible}
                score={HistoryscoreState}
                Type={HistoryTypeState}
                TypePerson={HistoryPersonType}
                Date={HistoryDateState}
                onClose={SwitchVisible}
            />
            <_renderSuggestion />
            <_renderHistory />
        </View>


    )


}
const styles = StyleSheet.create({
    cards: {
        width: 400,
        // height: 200,
        backgroundColor: '#E3A39A',
        marginLeft: 35,
        borderRadius: 15,
        marginTop: 25,
        elevation: 10,
        paddingHorizontal: 15
    },
    cardStat: {
        width: 347,
        height: 180,
        backgroundColor: '#FFFFFF',
        marginLeft: 5,
        borderRadius: 15,
        marginTop: 25,
        elevation: 10,
        paddingHorizontal: 39
    },
    titleStat: (success) => ({
        fontSize: 30,
        color: success == true ? 'green' : 'red',
        fontWeight: 'bold'
    }),
    Wrapper: {
        marginHorizontal: 15,
        justifyContent: 'space-around',
        flexDirection: 'row'

    },
    HistoryHeaderText: {
        paddingLeft: 35,
        fontSize: 35,
        fontWeight: "bold",
        marginTop: 35,
        color: '#205072'
    },
    HistoryTitle: {
        // marginHorizontal: "30%",
        marginTop: 10,
        fontSize: 30,
        fontWeight: '700'
    },
    HistoryTitleLearn: {
        // marginHorizontal: "10%",
        marginTop: 10,
        fontSize: 30,
        fontWeight: '700'
    },
    typePersonStat: {
        fontSize: 16,
        color: '#C1E8BB',
        fontWeight: "500",
        paddingLeft: 30
    },
    ModalCenter: {
        alignItems: 'center',
        marginTop: 20
    },
    textTypePerson: {
        fontSize: 16,
        color: '#C1E8BB',
        textAlign: 'right'
    },
    textDate: {
        fontSize: 13,
        color: '#000000',
        textAlign: 'right'
    },
    circleGO: {
        justifyContent: 'flex-end',
        marginBottom: 15,
        borderColor: "#FFFFFF",
        borderWidth: 1,
        borderRadius: 30,
        width: 33,
        height: 33,
        backgroundColor: '#FFFFFF'
    }
})
export default HomeScreen;