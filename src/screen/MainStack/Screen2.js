import React, { useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableHighlight,
} from 'react-native';

const Screen2 = (props) => {
    const { navigation } = props;
    return (
        <View>
            <Text> Screen 2</Text>
            <TouchableHighlight onPress={()=>{
                navigation.goBack();
            }}>
                <Text>Click</Text>
            </TouchableHighlight>
        </View >
    );
};





const styles = StyleSheet.create({
    Name: {
        fontSize: 16
    }
})
export default Screen2;