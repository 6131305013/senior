import React, { useState, useEffect, useCallback } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableHighlight,
} from 'react-native';
import handleGetPratice from '../../api/handleGetPractice';
import { Quiz } from '../../components';
import { scale, verticalScale, moderateScale } from '../../configs/scale';

const Practice = (props) => {
    const { navigation } = props
    const { type } = props.route.params;
    const [getPratice] = handleGetPratice();
    const [isQuiz, setQuiz] = useState([]);
    useEffect(() => {
        loadData();
        console.log('QUIZ : ', isQuiz)
    }, [])

    const loadData = async () => {
        let pratice = await getPratice(type);
        let convertQuiz = convertToQuizContainer(pratice);
        console.log('convertQuiz ', convertQuiz)
        setQuiz(convertQuiz)
    }


    const convertToQuizContainer = (arrays) => {
        let quiz = [];
        for (let i in arrays) {
            let CorrectAnswer = null;
            let answer = arrays[i].answer;
            let Answer = [];
            for (let j in answer) {
                if (answer[j].correct === true) {
                    CorrectAnswer = answer[j].title;
                }
                Answer.push(answer[j].title)
            }
            let question = {
                Question: arrays[i].question,
                Correct: CorrectAnswer,
                Choice: Answer
            }
            quiz.push(question)
        }
        return quiz
    }



    const Question = () => {
        return (
            <View>
                <Quiz
                    Data={isQuiz}
                // onFinish={}
                />
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <View>
                <View>
                    <TouchableHighlight onPress={() => navigation.navigate('Learning')}>
                        <Text>Go to Learning Screen</Text>
                    </TouchableHighlight>
                </View>
                <Question />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    }
})
export default Practice;