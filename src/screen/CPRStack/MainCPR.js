import React, { useEffect, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableHighlight,
    ScrollView,
    FlatList,
    TouchableOpacity
} from 'react-native';
import mockup from '../../configs/mockup'
import EvilIcons from "react-native-vector-icons/EvilIcons";
import AntDesign from "react-native-vector-icons/AntDesign";
import handleCreateCPR from '../../api/handleCreateCPR';
import handleGetCPR from '../../api/handleGetCPR';
import handleUpdateCPR from '../../api/handleUpdateCPR';
import { scale, verticalScale, moderateScale } from '../../configs/scale'
import moment from 'moment'
const numColumns = 2

const MainCPR = (props) => {
    const { id,
        firstname,
        lastname,
        age,
        status,
        userId } = props.route.params
    const { navigation } = props;
    const [createCPR] = handleCreateCPR();
    const [getAllcpr, getCprByID] = handleGetCPR();
    const [updateCPR] = handleUpdateCPR();
    const [CPRCase, setCPRcase] = useState();
    const [round, SetRound] = useState(false);

    useEffect(() => {
        let unsubscribe = navigation.addListener('focus', () => {
            loadData();
        });
        return unsubscribe;
    }, [navigation])


    const loadData = async () => {
        let CPR = await getCprByID(id, userId);
        console.log("Cpr : ", CPR);
        for (let i in CPR) {
            console.log(CPR[i].cpr_round)
            SetRound(CPR[i].cpr_round);
        }
        setCPRcase(CPR);
    }

    const submitCPR = async () => {

        let newCPR = await createCPR(body)
    }

    const _rederHistoryCPR = ({ item }) => {
        return (
            <View style={styles.cards}>
                <View style={{ flexDirection: 'row', marginHorizontal: "5%", marginVertical: "5%" }}>
                    <View>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#B8BCBE', textAlign: 'center' }}>ROUND</Text>
                        <Text style={{ fontSize: 50, fontWeight: 'bold', color: '#B8BCBE', textAlign: 'center' }}>{item.cpr_round}</Text>
                        <View style={{ marginButtom: moderateScale(15) }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#B8BCBE' }}>Time {moment(parseInt(item.createdAt)).format('LT')}</Text>
                        </View>
                    </View>

                    <View style={{ justifyContent: 'center', marginVertical: "8%" }}>
                        <ScrollView>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 18, marginHorizontal: "10%", fontWeight: 'bold' }}>EKG:</Text>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#77B7D6' }}>{item.cpr_EKG}</Text>
                            </View>

                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 18, marginHorizontal: "10%", fontWeight: 'bold' }}>Medicine:</Text>
                                <View>
                                    {item.cpr_medicine_total != null ?
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#881717' }}>{item.cpr_medicine_total}</Text> :
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#881717' }}>0</Text>
                                    }
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 18, marginLeft: moderateScale(15), fontWeight: 'bold' }}>Defib:</Text>
                                <View>
                                    {
                                        item.cpr_Defib == true ?
                                            <EvilIcons name="check" size={32} color={"green"} />
                                            :
                                            <EvilIcons name="close-o" size={32} color={"red"} />
                                    }

                                </View>
                            </View>
                        </ScrollView>

                    </View>

                </View>
            </View>
        )
    }


    return (
        <View style={{ flex: 1 }}>
            <ScrollView>
                <View style={{ backgroundColor: "#C8E2C9", height: verticalScale(180), width: '95%', borderRadius: moderateScale(15), elevation: 10, marginHorizontal: moderateScale(20), marginVertical: moderateScale(20) }}>

                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ paddingLeft: moderateScale(42), paddingTop: moderateScale(24) }}>
                            <Text style={{ fontSize: 15, color: "#205072" }}>Case Name</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 30, color: "#205072", fontWeight: "bold" }}>{firstname},</Text>
                                <Text style={{ fontSize: 30, color: "#205072", }}>{lastname}</Text>
                            </View>
                        </View>
                        <View style={{ paddingLeft: '30%', paddingTop: moderateScale(24) }}>
                            <Text style={{ fontSize: 15, color: "#205072" }}>Age</Text>
                            <Text style={{ fontSize: 30, color: "#205072", fontWeight: "bold", }}>{age}</Text>
                        </View>
                        <View style={{ paddingLeft: '20%', paddingTop: moderateScale(24) }}>
                            <Text style={{ fontSize: 15, color: "#205072" }}>Status</Text>
                            <Text style={{ fontSize: 30, color: "#278D38", fontWeight: "bold", }}>{status}</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginRight: '10%' }}>
                        {/* <TouchableOpacity onPress={() => {
                            navigation.navigate('CPR', { caseID: id, Round: round })
                        }}>
                            <View style={{ backgroundColor: "#61977A", height: '57%', width: '120%', borderRadius: 15, flexDirection: 'row', alignItems: 'center', marginVertical: 5 }}>
                                <View style={{ backgroundColor: "#FFFFFF", borderRadius: 45, height: 45, width: 45, alignItems: 'center', marginHorizontal: 15 }}>
                                    <AntDesign name="plus" size={35} color="#62A581" style={{ alignSelf: "center", marginTop: 4 }} />
                                </View>
                                <Text style={{ fontSize: 30, color: "#F5F5F5", textAlign: "center", fontWeight: "bold" }}>Add</Text>
                            </View>
                        </TouchableOpacity> */}
                        <TouchableOpacity onPress={() => {
                            navigation.navigate('ListOfPatient')
                        }}>
                            <View style={{ backgroundColor: "#CC5D5D", height: '57%', width: '100%', borderRadius: moderateScale(15), marginHorizontal: moderateScale(10), marginVertical: moderateScale(5), marginLeft: moderateScale(50) }}>
                                <Text style={{ fontSize: 30, color: "#F5F5F5", textAlign: "center", fontWeight: "bold", marginVertical: moderateScale(10) }}>BACK</Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                </View>

                <View>
                    <Text style={{ fontSize: 35, color: "#205072", paddingLeft: moderateScale(65), fontWeight: "bold", paddingTop: moderateScale(24) }}>History</Text>
                </View>

                <View style={{ flex: 1 }}>
                    <ScrollView>
                        <FlatList
                            data={CPRCase}
                            renderItem={_rederHistoryCPR}
                            keyExtractor={item => `${item.id}`}
                            numColumns={numColumns}
                        />
                    </ScrollView>
                </View>
            </ScrollView>
        </View>

    )
}





const styles = StyleSheet.create({
    cards: {
        width: "42%",
        height: "90%",
        backgroundColor: '#FFFFFF',
        marginLeft: moderateScale(50),
        borderRadius: moderateScale(15),
        marginTop: moderateScale(25),
        elevation: 10,
        paddingHorizontal: moderateScale(15)
    }
})
export default MainCPR;