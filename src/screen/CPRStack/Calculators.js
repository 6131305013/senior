import React, { useEffect, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableWithoutFeedback,
    ScrollView,
    FlatList,
    TouchableOpacity,
    Modal,
    TextInput
} from 'react-native';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AntDesign from "react-native-vector-icons/AntDesign";
import { COLORS, SIZES, FONTS } from '../../themes';
import CustomHeader from '../../navigation/CustomHeader';
import { scale, verticalScale, moderateScale } from '../../configs/scale';


const numColumns = 2

const Calculators = (props) => {
    const { navigation } = props;
    // const { caseID, Round } = props.route.params;

    return (
        <View style={styles.container}>
            <CustomHeader title={'Calculators'} />
            <View style={[styles.cards, { height: verticalScale(200), marginVertical: "5%" }]}>
                <View style={{ marginHorizontal: "5%", marginVertical: "3%" }}>
                    <View style={{ flexDirection: "row", marginVertical: "3%" }}>
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>Age </Text>
                        <TextInput
                            placeholder="age"
                            onChangeText={(defib) => setDescriptDe(defib)}
                            keyboardType="numeric"
                            style={{
                                height: verticalScale(45),
                                width: '30%',
                                borderRadius: moderateScale(10),
                                backgroundColor: '#E4E3E9',
                                fontSize: SIZES.h2,
                                marginHorizontal: '2%',
                            }} />
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}> Years</Text>
                    </View>

                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>Weight </Text>
                        <TextInput
                            placeholder="weight"
                            onChangeText={(defib) => setDescriptDe(defib)}
                            keyboardType="numeric"
                            style={{
                                height: verticalScale(45),
                                width: '30%',
                                borderRadius: moderateScale(10),
                                backgroundColor: '#E4E3E9',
                                fontSize: SIZES.h2,
                                marginHorizontal: '2%',
                            }} />
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}> Kg</Text>
                    </View>
                </View>
            </View>

            <View style={{ marginHorizontal: "4%" }}>
                <Text style={{ fontSize: 45, fontWeight: "700" }}>Result</Text>
            </View>

            <View style={[styles.cards, { height: '42%', marginVertical: "3%" }]}>
                <View style={{ marginVertical: '2%' }}>

                    <View style={{ marginHorizontal: '5%', marginVertical: '1%' }}>
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>Uncuffed Tube Size</Text>

                    </View>

                    <View style={styles.divider}></View>

                    <View style={{ marginHorizontal: '5%' }}>
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>Cuffed Tube Size</Text>
                    </View>

                    <View style={styles.divider}></View>

                    <View style={{ flexDirection: 'row', marginHorizontal: '5%' }}>
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>Adrenaline</Text>
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>        mg(1:10,00)/</Text>
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>        ml(1:10,000)</Text>
                    </View>

                    <View style={styles.divider}></View>

                    <View style={{ flexDirection: 'row', marginHorizontal: '5%' }}>
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>Amiodarone</Text>
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>        mg</Text>
                    </View>

                    <View style={styles.divider}></View>

                    <View style={{ flexDirection: 'row', marginHorizontal: '5%' }}>
                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>NSS</Text>

                        <Text style={{ fontSize: SIZES.largeTitle, fontWeight: '700' }}>        ml/kg</Text>
                    </View>

                </View>
            </View>

            <TouchableOpacity onPress={async () => {
                navigation.navigate('CPR', { caseID: caseID, Round: Round })
            }}>
                <View style={styles.button}>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 50, fontWeight: '700', color: COLORS.white }}>CPR</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
}





const styles = StyleSheet.create({
    cards: {
        backgroundColor: COLORS.white,
        borderRadius: moderateScale(10),
        marginHorizontal: "4%",
    },
    container: {
        flex: 1,
        backgroundColor: COLORS.BG
    },
    divider: {
        borderWidth: moderateScale(1),
        borderColor: '#BFBFBF',
        marginLeft: "5%",
        marginVertical: '3%'
    },
    button: {
        backgroundColor: COLORS.On,
        borderRadius: moderateScale(10),
        marginHorizontal: "4%",
        height: '30%',
        justifyContent: 'center'
    }
})
export default Calculators;