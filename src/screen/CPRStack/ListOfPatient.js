import React, { useEffect, useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableWithoutFeedback,
    ScrollView,
    FlatList,
    TouchableOpacity,
    Modal,
    TextInput
} from 'react-native';
import mockup from '../../configs/mockup'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Child from "react-native-vector-icons/MaterialIcons";
import Adult from "react-native-vector-icons/MaterialCommunityIcons";
import handleCreateCase from '../../api/handleCreateCase';
import handleGetCase from '../../api/handleGetCase';
import handleGetCPR from '../../api/handleGetCPR';
import CustomHeader from '../../navigation/CustomHeader';
import handleGetActivity from '../../api/handleGetActivity';
import handleUpdateCase from '../../api/handleUpdateCase';
import { scale, verticalScale, moderateScale } from '../../configs/scale'
import { COLORS } from '../../themes';
import { Button } from 'react-native';


const numColumns = 2

const ListOfPatient = (props) => {
    const { navigation } = props;
    const [modalVisible, setModalVisible] = useState(false);
    const { user } = mockup;
    const [updateCase] = handleUpdateCase();
    const [number, onChangeNumber] = React.useState(null);
    const [createCase] = handleCreateCase();
    const [getAllcase, getCaseByID, getCaseByUserId] = handleGetCase();
    const [getAllActivity, getActivityByUIDandCID] = handleGetActivity();
    const [getAllcpr, getCprByID] = handleGetCPR();
    const [caseState, setCase] = useState();
    const [FName, setFName] = useState();
    const [LName, setLName] = useState();
    const [Status, setStatus] = useState();
    const [SelectID, SetSelectID] = useState();
    const [Age, setAge] = useState();
    const [Lang, setLang] = useState('TH');

    useEffect(() => {
        loadCase();

    }, [])
    const loadCase = async () => {
        (async () => {
            // console.log("uid", user.id)
            let Case = await getCaseByUserId(user.id);
            // console.log("case : ", Case);
            setCase(Case);
        })();
    }

    const UpdateCase = async () => {
        let body = {
            case_id: SelectID,
            firstname: FName,
            lastname: LName,
            status: Status,
            Languages: "TH",
            age: Age
        }
        await updateCase(body).then((res) => {
            loadCase();
            setModalVisible(!modalVisible);
        })
    }

    const onClickToGoSummary = async (Data) => {
        let CPRList = await getCprByID(Data.case_id, user.id);
        let getActivity = await getActivityByUIDandCID(user.id, Data.case_id);
        // console.log('getCprByID : ', CPRList)
        // console.log('getActivity : ', getActivity)
        navigation.navigate('summary', {
            isCompleteCPR: CPRList,
            UserData: Data,
            ActivityLog: getActivity.data,
            VentilatorState: getActivity.VentilatorRound,
            countRound: getActivity.Round,
            date: getActivity.date,
            CaseId: Data.case_id
        })
    }


    const _rederListPatients = ({ item }) => {
        // console.log(item)
        return (
            <TouchableWithoutFeedback onPress={() => {
                if (item.case_firstname != null) {
                    onClickToGoSummary(item)
                } else {
                    setStatus(item.case_status)
                    SetSelectID(item.case_id)
                    setModalVisible(!modalVisible)
                }
            }}>
                <View style={styles.cards}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginHorizontal: 50 }}>
                        <View style={[{ paddingTop: moderateScale(15), flexGrow: 1, flexShrink: 0, flexBasis: 160, alignItems: 'flex-start' }]}>
                            {item.case_firstname != null ?
                                <View>
                                    <Text style={{ fontSize: 20, color: COLORS.Header }}>Name</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 30, color: COLORS.Header, fontWeight: "bold" }}>{item.case_firstname},</Text>
                                        <Text style={{ fontSize: 30, color: COLORS.Header, }}>{item.case_lastname}</Text>
                                    </View>
                                </View>
                                :
                                <View>
                                    <Text style={{ fontSize: 20, color: COLORS.Header }}>Case ID</Text>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        {item.case_firstname == null && <MaterialIcons name="warning" size={35} color={COLORS.red} />}
                                        <Text style={{ fontSize: 35, color: COLORS.Header, fontWeight: "bold", marginLeft: 10 }}>{item.case_id}</Text>
                                    </View>
                                </View>
                            }
                        </View>

                        <View style={[{ paddingTop: moderateScale(15), alignItems: 'center' }, styles.BoxInList]}>
                            {item.case_Age != 0 ? <View><Text style={{ fontSize: 20, color: COLORS.Header }}>Age</Text>
                                <Text style={{ fontSize: 30, color: COLORS.Header, fontWeight: "bold", }}>{item.case_Age}
                                </Text>
                            </View>
                                :
                                <View>
                                    <Text style={{ fontSize: 25, color: COLORS.Header, fontWeight: "bold", marginTop: moderateScale(10) }}>Please Input infomation</Text>
                                </View>
                            }
                        </View>

                        <View style={[{ paddingTop: moderateScale(15), alignItems: 'flex-end' }, styles.BoxInList]}>
                            <Text style={{ fontSize: 20, color: COLORS.Header }}>Status</Text>
                            <Text style={{ fontSize: 30, color: "#278D38", fontWeight: "bold", }}>{item.case_status}</Text>
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', marginLeft: "55%" }}>

                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }


    return (
        <View style={styles.container}>
            <CustomHeader
                title={'List of Patients'} />
            <Modal
                animationType='fade'
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => onClose()}
            >
                <View style={styles.PositionModal}>
                    <View style={styles.modalCard}>
                        <View style={styles.ContentModal}>
                            <View style={{ marginHorizontal: "2%" }}>
                                <Text style={{ fontSize: 25, fontWeight: 'bold', color: COLORS.Header }}>Case Name:</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <TextInput
                                    style={styles.InputFName}
                                    onChangeText={(firstname) => setFName(firstname)}
                                    placeholder="firstname" />
                                <TextInput
                                    style={styles.InputLName}
                                    onChangeText={(lastname) => setLName(lastname)}
                                    placeholder="lastname" />
                            </View>
                            <View style={{ flexDirection: 'row', marginVertical: "3%", marginHorizontal: "2%" }}>
                                <View>
                                    <Text style={{ fontSize: 25, fontWeight: 'bold', color: COLORS.Header }}>Age:</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <TextInput
                                    style={styles.InputAge}
                                    onChangeText={(age) => setAge(age)}
                                    placeholder="age"
                                    value={number}
                                    keyboardType="numeric" />

                                <View >
                                    <TouchableOpacity onPress={() => UpdateCase()} >
                                        <View style={styles.Summit}>
                                            <Text style={{ fontSize: 30, fontWeight: 'bold', color: COLORS.white }}> Summit </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>

                            </View>

                        </View>
                    </View>
                </View>

            </Modal>
            <ScrollView>
                <View style={{ marginTop: moderateScale(20), flexDirection: 'row', justifyContent: 'center', width: "100%" }}>
                    <TouchableOpacity onPress={async () => {

                        let body = {
                            userId: user.id,
                            firstname: null,
                            lastname: null,
                            status: 'NEXT',
                            age: 0,
                            Languages: Lang,
                        }
                        let newCase = await createCase(body)
                        // console.log('ID : ', newCase)
                        navigation.navigate('CPR', { caseID: newCase.id, Round: 0 })
                    }}>

                        <View style={[{ flexDirection: 'row' }, styles.QUICK]}>
                            <Text style={{ fontSize: 30, fontWeight: 'bold', color: COLORS.white }}>QUICK CPR</Text>
                            <Child name="child-care" size={40} color={COLORS.white} style={{ marginHorizontal: '2%' }} />
                        </View>

                    </TouchableOpacity>

                    <TouchableOpacity onPress={async () => {

                        let body = {
                            userId: user.id,
                            firstname: null,
                            lastname: null,
                            status: 'NEXT',
                            age: 0,
                            Languages: Lang,
                        }
                        let newCase = await createCase(body)
                        // console.log('ID : ', newCase)
                        navigation.navigate('CPR', { caseID: newCase.id, Round: 0 })
                    }}>

                        <View style={[{ flexDirection: 'row' }, styles.QUICK]}>
                            <Text style={{ fontSize: 30, fontWeight: 'bold', color: COLORS.white }}>QUICK CPR</Text>
                            <Adult name="human-male-female" size={40} color={COLORS.white} style={{ marginHorizontal: '2%' }} />
                        </View>

                    </TouchableOpacity>

                </View>
                <View >
                    <FlatList
                        data={caseState}
                        renderItem={_rederListPatients}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </ScrollView>
        </View>
    )
}



const styles = StyleSheet.create({
    cards: {
        flex: 1,
        backgroundColor: COLORS.list,
        height: verticalScale(70),
        borderRadius: moderateScale(10),
        marginHorizontal: moderateScale(15),
        marginVertical: "1%"
    
    },
    container: {
        flex: 1,
        backgroundColor: COLORS.BG
    },
    PositionModal: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginVertical: "12%"
    },
    ContentModal: {
        alignSelf: 'center',
        marginVertical: '8%',
    },
    QUICK: {
        backgroundColor: COLORS.quick,
        borderRadius: moderateScale(10),
        height: verticalScale(70),
        width: scale(158),
        marginHorizontal: "1%",
        marginVertical: "1%",
        justifyContent: 'center',
        alignItems: 'center',
    },
    BoxInList: {
        flexGrow: 1,
        flexShrink: 0,
        flexBasis: 40,
    },
    modalCard: {
        backgroundColor: COLORS.white,
        borderRadius: moderateScale(10),
        width: '75%',
        height: verticalScale(200)
    },
    InputFName: {
        height: verticalScale(35),
        width: "40%",
        borderRadius: moderateScale(10),
        marginHorizontal: "2%",
        backgroundColor: COLORS.input,
        fontSize: 24,
    },
    InputLName: {
        height: verticalScale(35),
        width: "40%",
        borderRadius: moderateScale(10),
        backgroundColor: COLORS.input,
        fontSize: 24
    },
    InputAge: {
        height: verticalScale(35),
        width: "20%",
        borderRadius: moderateScale(10),
        marginHorizontal: "2%",
        backgroundColor: COLORS.input,
        fontSize: 24
    },
    Summit: {
        width: '100%',
        height: verticalScale(35),
        backgroundColor: COLORS.On,
        borderRadius: moderateScale(10),
        justifyContent: 'center',
        marginHorizontal: '3%',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    }
})
export default ListOfPatient;