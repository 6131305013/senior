import React, { useEffect, useState, useCallback } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableWithoutFeedback,
    Button,
    Modal,
    FlatList,
    ScrollView,
    TouchableOpacity,
    TextInput
} from 'react-native';
import AntDesign from "react-native-vector-icons/AntDesign";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import handleGetMedicine from '../../api/handleGetMedicine';
import handleExportSheets from '../../api/handleExportSheets';
import moment, { now } from 'moment';
import mockup from '../../configs/mockup';
import { Res } from '../../configs';
import { COLORS } from '../../themes';
import { scale, verticalScale, moderateScale } from '../../configs/scale'
import handleUpdateCase from '../../api/handleUpdateCase';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';


const summary = (props) => {
    const { navigation } = props;
    const { isCompleteCPR, VentilatorState, countRound, ActivityLog, CaseId, UserData } = props.route.params;
    const { user } = mockup;
    const [getMedicine] = handleGetMedicine();
    const [updateCase] = handleUpdateCase();
    const [Medicine, setMedicine] = useState([]);
    const [Firstname, setFirstname] = useState("");
    const [Lastname, setLastname] = useState("");
    const [Status, setStatus] = useState("");
    const [Age, setAge] = useState(0);
    const [tableHead, settableHead] = useState(['Activity', 'Name', 'Time']);
    const [ActLog, setActLog] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);


    const [CPRList, setCPRList] = useState({});
    const [VentilatorRound, setVentilatorRound] = useState({
        Round: 0,
        Time: new Date(),
        used: false
    });
    const [Round, setRound] = useState(0)

    useEffect(() => {
        console.log('isCompleteCPR : ', props.route.params)
        setCPRList(isCompleteCPR);
        setVentilatorRound(VentilatorState)
        setRound(countRound);
        setMedicineFormat();
        setActLog(ActivityLog);
        for (let i in isCompleteCPR) {
            if (isCompleteCPR[i].status == 'ROSC') {
                setStatus('SAFETY')
            } else {
                setStatus('DEAD')
            }
        }
    }, [])
    const setMedicineFormat = async () => {
        let Medicine = await getMedicine();
        // console.log("Medicine:", Medicine)
        let MedFormatState = [];
        for (let i in Medicine) {
            let Select = { Select: false }
            let object = Object.assign(Medicine[i], Select)
            MedFormatState.push(object)
        }
        setMedicine(MedFormatState);
    }

    const TextMedicine = (item) => {
        let text = '';
        for (let i in item) {
            let getMed = Medicine;
            for (let m in getMed) {
                if (m == 0) {
                    text = getMed[m].med_name
                } else {
                    if (item[i] == getMed[m].med_code) {
                        text = text + ',' + getMed[m].med_name
                    }
                }
            }
        }
        return text
    }

    const _renderHeaderSum = () => {
        return (
            <View style={{
                backgroundColor: 'white',
                borderWidth: moderateScale(2),
                borderRadius: moderateScale(10),
                borderColor: '#29C91E',
                justifyContent: 'center',
                alignItems: 'center',
                marginHorizontal: moderateScale(10),
                marginVertical: moderateScale(10)
            }}>
                <View style={{
                    flexDirection: 'row',
                    borderWidth: moderateScale(2),
                    borderRadius: moderateScale(25),
                    borderColor: 'transparent',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginHorizontal: moderateScale(10),
                    marginVertical: moderateScale(10)
                }}>
                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Name : </Text>
                            <View>
                                {UserData.case_firstname != null && UserData.case_lastname != null ?
                                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{UserData.case_firstname} {UserData.case_lastname} </Text> :
                                    UserData.case_firstname == " " && UserData.case_lastname == " " ?
                                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Unknown</Text> : <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Unknown</Text>
                                }
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginVertical: '3%' }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Age : </Text>
                            <View>
                                {UserData.case_Age != 0 ?
                                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{UserData.case_Age}  </Text> : <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{UserData.case_Age}  </Text>}
                            </View>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Status : </Text>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{Status}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Image source={Res.Image.Ventilator} style={{ width: '50%', height: '50%' }} />
                        <View>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>At</Text>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Round {VentilatorRound.Round}</Text>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{moment(parseInt(VentilatorRound.Time)).format('HH:MM:SS')} </Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    const GetColorInStatus = (status) => {
        if (status == "NEXT") {
            return COLORS.nextR
        } else if (status == "") {
            return "Black"
        } else {
            return COLORS.rosc
        }
    }

    const updateCaseAndGoBack = async () => {
        let body = {
            case_id: CaseId,
            firstname: UserData.case_firstname != null ? UserData.case_firstname : null,
            lastname: UserData.case_lastname != null ? UserData.case_lastname : null,
            status: Status,
            Languages: "TH",
            age: UserData.case_Age != null ? UserData.case_Age : null
        }
        if (UserData.case_firstname == null && UserData.case_lastname == null) {
            await updateCase(body).then((res) => {
                navigation.replace('ListOfPatient')
            })
        } else {
            navigation.replace('ListOfPatient')
        }
    }

    const _renderListCPR = ({ item, index }) => {
        return (
            <View style={[{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: moderateScale(10),
                marginVertical: moderateScale(5),
                paddingHorizontal: moderateScale(15),
                borderWidth: moderateScale(5),
                backgroundColor: GetColorInStatus(item.status),
                borderColor: GetColorInStatus(item.status),
                borderRadius: moderateScale(10)
            }]}>
                <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{index + 1}</Text>
                </View>
                <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{moment(item.Time).format('HH:MM:SS')}</Text>
                </View>
                <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{item.EKG}</Text>
                </View>
                <View style={{ flexGrow: 1, flexBasis: 150, alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{TextMedicine(item.Medcine)}</Text>
                </View>
                <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                    {item.status === 'NEXT' ? null : <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{item.status}</Text>}
                </View>
            </View>
        )
    }

    return (
        <View>
            <ScrollView>
                <View>
                    <View>
                        <View style={{ alignItems: 'center' }}>
                            <View style={{ marginVertical: '3%' }}>
                                <Text style={{ color: COLORS.Header, fontSize: 45, fontWeight: 'bold' }}>Result</Text>
                            </View>

                            <View style={{
                                backgroundColor: COLORS.white,
                                borderWidth: moderateScale(2),
                                borderRadius: moderateScale(10),
                                borderColor: COLORS.white,
                                marginHorizontal: moderateScale(10),
                                marginVertical: moderateScale(10),
                                height: '100%',
                                width: '90%',
                                elevation: 10,
                            }}>
                                <_renderHeaderSum />
                                <View style={{
                                    flexDirection: 'row',
                                    marginHorizontal: moderateScale(10),
                                    justifyContent: 'space-between'
                                }}>
                                    <TouchableOpacity onPress={() => {
                                        // console.log(Round)
                                    }}>
                                        <View style={{
                                            backgroundColor: COLORS.ActiLog,
                                            borderColor: COLORS.ActiLog,
                                            borderWidth: moderateScale(1),
                                            borderRadius: moderateScale(10),
                                            width: scale(150),
                                            height: verticalScale(30),
                                            alignContent:"center",
                                            alignItems:'center',
                                            justifyContent:'center'
                                        }}>
                                            <Text style={{ color: COLORS.white, fontSize: 25, fontWeight: 'bold' }}>Activity LOG</Text>
                                        </View>
                                    </TouchableOpacity>

                                </View>

                                <View style={{ top: -40, marginLeft: '60%' }}>
                                    <Text style={{ color: COLORS.black, fontSize: 25, fontWeight: 'bold' }}>Total Round : {Round} Round</Text>
                                </View>
                                <View>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: '3%' }}>
                                            <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                                                <Text style={[styles.TextRe]}>No</Text>
                                            </View>
                                            <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                                                <Text style={[styles.TextRe]}>Time</Text>
                                            </View>
                                            <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                                                <Text style={[styles.TextRe]}>EKG</Text>
                                            </View>
                                            <View style={{ flexGrow: 1, flexBasis: 150, alignItems: 'center' }}>
                                                <Text style={[styles.TextRe]}>Medicine</Text>
                                            </View>
                                            <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                                                <Text style={[styles.TextRe]}>Status</Text>
                                            </View>

                                        </View> */}

                                        {/* CPRList */}
                                        {/* <FlatList
                                            data={CPRList}
                                            renderItem={_renderListCPR}
                                            keyExtractor={(item, index) => item.Time}
                                            ListEmptyComponent={() => {
                                                return (
                                                    <View></View>
                                                )
                                            }}
                                        /> */}


                                        {/* Activity Log */}
                                        <View style={{ width: '90%', height: verticalScale(260), marginVertical: moderateScale(10) }}>
                                            <Table borderStyle={{ borderWidth: moderateScale(1), borderColor: '#000000' }}>
                                                <Row data={tableHead} style={styles.head} textStyle={styles.text} />
                                            </Table>
                                            <ScrollView style={styles.dataWrapper}>
                                                <Table borderStyle={{ borderWidth: moderateScale(1), borderColor: '#000000' }}>
                                                    <Rows data={ActLog} textStyle={styles.text} style={styles.dataText} />
                                                </Table>
                                            </ScrollView>
                                        </View>

                                        <Modal
                                            animationType='fade'
                                            visible={modalVisible}
                                            transparent={true}
                                            onRequestClose={() => onClose()}
                                        >
                                            <View style={styles.PositionModal}>
                                                <View style={styles.modalCard}>
                                                    <View style={styles.ContentModal}>
                                                        <View style={{ marginHorizontal: "2%", flexDirection: 'row', marginVertical: "1%" }}>
                                                            <Text style={{ fontSize: 25, fontWeight: 'bold', color: COLORS.Header }}>Gmail:</Text>
                                                            <TextInput
                                                                style={styles.Gmail}
                                                                placeholder="Please Input your Gmail!" />
                                                        </View>

                                                        <View style={{ flexDirection: 'row', marginVertical: "5%", justifyContent: 'center', alignContent: 'space-between' }}>
                                                            <View>
                                                                <TouchableOpacity >
                                                                    <View style={styles.Send}>
                                                                        <Text style={{ fontSize: 30, fontWeight: 'bold', color: COLORS.white }}> SEND </Text>
                                                                    </View>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View >
                                                                <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
                                                                    <View style={styles.Cancel}>
                                                                        <Text style={{ fontSize: 30, fontWeight: 'bold', color: COLORS.white }}> CANCEL </Text>
                                                                    </View>
                                                                </TouchableOpacity>
                                                            </View>

                                                        </View>
                                                    </View>
                                                </View>
                                            </View>

                                        </Modal>

                                    </View>
                                    <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
                                        <View style={{
                                            backgroundColor: COLORS.On,
                                            alignItems: 'center',
                                            borderColor: COLORS.On,
                                            borderWidth: moderateScale(1),
                                            borderRadius: moderateScale(10),
                                            marginVertical: "1%",
                                            height:verticalScale(30),
                                            justifyContent:'center'
                                        }}>
                                            <Text style={{ color: COLORS.white, fontSize: 20, fontWeight: 'bold' }}>EXPORT</Text>
                                        </View>

                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => updateCaseAndGoBack()}>
                                        <View style={{
                                            backgroundColor: COLORS.black,
                                            alignItems: 'center',
                                            borderColor: COLORS.black,
                                            borderWidth: moderateScale(1),
                                            borderRadius: moderateScale(10),
                                            height:verticalScale(30),
                                            justifyContent:'center'
                                        }}>
                                            <Text style={{ color: COLORS.white, fontSize: 20, fontWeight: 'bold' }}>Finish</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    background: {
        backgroundColor: COLORS.BG,
        borderBottomLeftRadius: moderateScale(34),
        borderBottomRightRadius: moderateScale(34),
        marginBottom: '5%',
        elevation: 15
    },
    cards: {
        backgroundColor: COLORS.white,
        borderRadius: moderateScale(30),
        marginHorizontal: '1%',
        height: moderateScale(60),
        width: '20%',
        justifyContent: 'center'
    },
    cardsDefib: {
        borderRadius: moderateScale(30),
        marginLeft: '3%',
        height: '30%',
        width: '30%',
        justifyContent: 'center'
    },
    PositionModal: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    medicineModal: (item) => ({
        backgroundColor: item.Select ? "#62A581" : "#FF91BB",
        height: verticalScale(70),
        width: '30%',
        borderRadius: moderateScale(10),
        marginHorizontal: '1%',
        marginVertical: '1%'
    }),
    GetColorByStatus: (status) => ({
        backgroundColor:
            status == 'NEXT' ? COLORS.nextR :
                status == 'DEAD' ? COLORS.black :
                    COLORS.rosc
    }),
    TextRe: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    dataWrapper: {
        marginTop: moderateScale(-1)
    },
    inputText: {
        borderRadius: moderateScale(15),
        backgroundColor: COLORS.white,
        borderWidth: moderateScale(1),
        top: -5
    },
    dataWrapper: {
    },
    head: {
        height: verticalScale(20),
        backgroundColor: COLORS.rosc,
    },
    dataText: {
        height: verticalScale(20),
        backgroundColor: COLORS.white,
    },
    text: {
        margin: moderateScale(6),
        color: "#000000",
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20
    },
    PositionModal: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginVertical: "12%"
    },
    ContentModal: {
        justifyContent: "center",
        alignSelf: 'center',
        marginVertical: '10%',
    },
    modalCard: {
        backgroundColor: COLORS.white,
        borderRadius: moderateScale(10),
        width: '80%',
        height: verticalScale(150),
        borderWidth: moderateScale(1)
    },
    Gmail: {
        height: verticalScale(25),
        width: scale(150),
        borderRadius: moderateScale(10),
        backgroundColor: COLORS.input,
        fontSize: 18,
        marginHorizontal: "1%",
        alignItems:'center'
    },
    Send: {
        width: scale(70),
        height: verticalScale(25),
        backgroundColor: COLORS.On,
        borderRadius: moderateScale(10),
        justifyContent: 'center',
        marginHorizontal: '3%',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
    },
    Cancel: {
        width: scale(70),
        height: verticalScale(25),
        backgroundColor: COLORS.quick,
        borderRadius: moderateScale(10),
        justifyContent: 'center',
        marginHorizontal: '3%',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
    }
})
export default summary;