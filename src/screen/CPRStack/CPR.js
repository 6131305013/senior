import React, { useEffect, useState, useCallback } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Animated,
    Image,
    TouchableWithoutFeedback,
    Button,
    Modal,
    FlatList,
    ScrollView,
    TouchableOpacity,
    TextInput
} from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import handleGetMedicine from '../../api/handleGetMedicine';
import handleCreateCPR from '../../api/handleCreateCPR';
import handleDeleteCase from '../../api/handleDeleteCase';
import CountDown from 'react-native-countdown-component';
import handleCreateActivity from '../../api/handleCreateActivity';
import moment, { now } from 'moment';
import mockup from '../../configs/mockup';
import { scale, verticalScale, moderateScale } from '../../configs/scale'
import { COLORS, Res, SIZES, FONTS } from '../../configs';
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';



const CPR = (props) => {
    const { navigation } = props;
    const { caseID, Round } = props.route.params;
    const [deleteCase] = handleDeleteCase();
    const [createActivity] = handleCreateActivity();
    const { user } = mockup;
    const [getMedicine] = handleGetMedicine();
    const [createCPR] = handleCreateCPR();
    const [refresh, setRefresh] = useState(false);
    const [EKG, setEKG] = useState([
        {
            name: 'VF', Select: false
        },
        {
            name: 'VT', Select: false
        },
        {
            name: 'PEA', Select: false
        },
        {
            name: 'Asystole', Select: false
        },
    ]);
    const [med, setMed] = useState([]);
    const [Defib, setDefib] = useState(false);
    const [Medicine, setMedicine] = useState([]);
    const [Finish, SetFinish] = useState(false);
    const [isComplete, SetComplete] = useState(false);
    const [DescriptDe, setDescriptDe] = useState(null);
    const [isRound, setRound] = useState();
    const [countRound, setCountRound] = useState(1);
    const [date, setDate] = useState(false);
    const [VentilatorState, setVentilator] = useState({
        Round: 0,
        Time: now,
        used: false
    })
    const [isCompleteCPR, SetCompleteCPR] = useState([]);
    const [ActLog, setActLog] = useState([]);
    const [tableHead, settableHead] = useState(['Activity', 'Name', 'Time']);

    useEffect(() => {
        setDate(new Date())
        setMedicineFormat();
        setRound(Round + 1)
    }, [])

    const ActivityLog = async (activity, name, time) => {
        let array = [activity, name, moment(new Date()).format('HH:MM:SS')];
        let tempArray = ActLog;
        tempArray.push(array);
        setActLog(tempArray);
        let body = {
            activity: activity,
            name: name,
            userId: user.id,
            caseId: caseID
        }
        await createActivity(body);
    }


    const selectEKG = (sEKG) => {
        // console.log(' refresh : ', refresh)
        let newEKG = EKG;
        let nameEKG = null;
        let SelectEKGState = [];
        for (let i in newEKG) {

            if (newEKG[i].name === sEKG) {
                nameEKG = newEKG[i].name;
                newEKG[i].Select = true
            } else {
                newEKG[i].Select = false

            }
            SelectEKGState.push(newEKG[i])
        }
        // console.log(' Old EKG : ', EKG)
        // console.log(' New EKG : ', SelectEKGState)
        setEKG(SelectEKGState)
        ActivityLog('EKG', nameEKG, now);
        // console.log('New refresh : ', refresh)
    }

    const setMedicineFormat = async () => {
        let Medicine = await getMedicine();
        // console.log("Medicine:", Medicine)
        let MedFormatState = [];
        for (let i in Medicine) {
            let Select = { Select: false }
            let object = Object.assign(Medicine[i], Select)
            MedFormatState.push(object)
        }
        setMedicine(MedFormatState);
    }

    const setSelectMedicine = (item) => {
        // console.log('setSelectMedicine : ', item)
        let getMed = Medicine;
        let MedName = null;
        let MedSelect = [];
        for (let i in getMed) {
            let object = null;
            // console.log(getMed[i].med_code)
            if (getMed[i].med_code === item.med_code) {
                item.Select = !getMed[i].Select;
                MedName = getMed[i].med_name;
                object = item;
            } else {
                object = getMed[i]
            }
            MedSelect.push(object)
        }
        setMedicine(MedSelect);
        ActivityLog('Medicine', MedName, now);
        FinishMedicine()
    }

    const FinishMedicine = () => {
        let getMed = Medicine
        let MedState = [];
        for (let i in getMed) {
            let object = null;
            if (getMed[i].Select === true) {
                object = getMed[i];
                MedState.push(object)
            }
        }
        setMed(MedState);
    }

    const setHistoryCPR = (ekg, defib, medicine, status) => {
        let ALlData = isCompleteCPR;
        let newData = {
            Time: new Date(),
            EKG: ekg,
            Defib: defib,
            Medcine: medicine,
            status: status
        }
        ALlData.push(newData);
        SetCompleteCPR(ALlData);
    }

    const submitCPROrder = async (status) => {

        // console.log('submit Order ===>')
        let getMed = med;
        let sendObject = [];
        let ekg = null
        for (let i in EKG) {
            if (EKG[i].Select === true) {
                ekg = EKG[i].name
            }
        }
        for (let i in getMed) {
            sendObject.push(getMed[i].med_code)
        }
        setHistoryCPR(ekg, DescriptDe, sendObject, status);
        let body = {
            userID: user.id,
            caseID: caseID,
            medicine: sendObject,
            Defib: DescriptDe,
            status: status,
            EKG: ekg,
            Defib: Defib,
            Round: isRound
        }
        ActivityLog('CPR', status, now);
        await createCPR(body).then((res) => {
            // console.log('res ==> ', res)
            if (status === 'NEXT') {
                setCountRound(countRound + 1);
                NextRound();
            } else {
                navigation.navigate('summary', {
                    isCompleteCPR: isCompleteCPR,
                    VentilatorState: VentilatorState,
                    countRound: countRound,
                    date: date,
                    CaseId: caseID,
                    ActivityLog: ActLog,
                    UserData: { case_firstname: null, case_lastaname: null, case_Age: 0 }
                })
            }
        })

    }

    const NextRound = () => {
        setRound(isRound + 1)
        setMedicineFormat()
        setMed()
        setDescriptDe(null)
        setEKG([
            {
                name: 'VF', Select: false
            },
            {
                name: 'VT', Select: false
            },
            {
                name: 'PEA', Select: false
            },
            {
                name: 'A systole', Select: false
            },
        ])
        setDate(new Date())
        setRefresh(!refresh)
    }

    const TextMedicine = (item) => {
        let text = '';
        for (let i in item) {
            let getMed = Medicine;
            for (let m in getMed) {
                if (m == 0) {
                    text = getMed[m].med_name
                } else {
                    if (item[i] == getMed[m].med_code) {
                        text = text + ',' + getMed[m].med_name
                    }
                }
            }
        }
        return text
    }

    const UrgeWithPleasureComponent = useCallback(() => {
        return (
            <CountdownCircleTimer
                isPlaying={true}
                duration={10}
                colors={[
                    ['#61977A', 0.4],
                    ['#F7B801', 0.4],
                    ['#A30000', 0.2],
                ]}
                size={280}
                trailStrokeWidth={20}
                strokeWidth={20}
                onComplete={() => SetFinish(true)}
            >
                {({ remainingTime, animatedColor }) => (
                    <Animated.Text style={{ color: animatedColor, fontSize: 100 }}>
                        {remainingTime}
                    </Animated.Text>
                )}
            </CountdownCircleTimer>
        )
    }, [refresh])


    const _renderEKG = ({ item }) => {
        return (
            <TouchableWithoutFeedback onPress={() => selectEKG(item.name)}>
                <View style={[styles.cards, { backgroundColor: item.Select ? COLORS.select : COLORS.white }]} onPress={() => selectEKG(item.name)}>
                    <Text style={{ textAlign: 'center', fontSize: 30 }}>{item.name}</Text>
                </View>
            </TouchableWithoutFeedback>

        )
    }

    const _renderMedicine = ({ item }) => {
        return (
            <TouchableWithoutFeedback onPress={() => setSelectMedicine(item)}>
                <View style={[styles.medicineModal(item), { justifyContent: 'center', backgroundColor: item.Select ? COLORS.select : item.med_color, }]}>
                    <Text style={{ fontSize: 25, fontWeight: 'bold', textAlign: 'center' }}>{item.med_name}</Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }


    const GetColorInStatus = (status) => {
        if (status == "NEXT") {
            return "#F6D55C"
        } else if (status == "") {
            return "Black"
        } else {
            return "#95C798"
        }
    }

    const _renderListCPR = ({ item, index }) => {
        return (
            <View style={[{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: moderateScale(10),
                marginVertical: verticalScale(5),
                paddingHorizontal: moderateScale(15),
                borderWidth: scale(5),
                backgroundColor: GetColorInStatus(item.status),
                borderColor: GetColorInStatus(item.status),
                borderRadius: scale(10)
            }]}>
                <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{index + 1}</Text>
                </View>
                <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{moment(item.Time).format('HH:MM:SS')}</Text>
                </View>
                <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{item.EKG}</Text>
                </View>
                <View style={{ flexGrow: 1, flexBasis: 150, alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{TextMedicine(item.Medcine)}</Text>
                </View>
                <View style={{ flexGrow: 1, flexBasis: 10, alignItems: 'center' }}>
                    {item.status === 'NEXT' ? null : <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{item.status}</Text>}
                </View>
            </View>
        )
    }

    return (
        <View>

            {/* {
                isComplete ? */}
            {/* <_renderFinishCPR />  */}
            {/* : */}
            {/* <_renderCPR /> */}
            {/* } */}
            <View style={styles.background}>
                <TouchableWithoutFeedback onPress={async () => {
                    await deleteCase(caseID).then((res) => {
                        navigation.goBack()
                    })
                }}>
                    <View style={{ marginHorizontal: '2%', top: 50 }}>
                        <Ionicons name="chevron-back" size={45}>Back</Ionicons>
                    </View>
                </TouchableWithoutFeedback>

                <View style={{ marginHorizontal: '2%' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontSize: 36, fontWeight: 'bold', color: COLORS.Header, }}>Round </Text>
                            <Text style={{ fontSize: 60, fontWeight: 'bold', color: COLORS.Header, }}>{isRound}</Text>
                        </View>

                        <View>
                            <View style={{ alignItems: 'center', marginHorizontal: '25%' }}>
                                <Text style={{ fontSize: 30, fontWeight: 'bold', color: COLORS.Header }}>{moment(date).format('LTS')} </Text>
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                {/* ************************** Time ********************************** */}
                                <UrgeWithPleasureComponent />
                                {/* ************************** Time ********************************** */}
                            </View>
                        </View>

                    </View>

                    <View style={{ top: "-30%", flexDirection: 'row-reverse' }}>
                        <View style={{ alignItems: 'center' }}>

                            <Text style={{ fontSize: 30, fontWeight: 'bold', color: COLORS.Header, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>ET-Tube</Text>
                            <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                                <Image source={Res.Image.ETTube} style={{ width: scale(65), height: verticalScale(110) }} />
                                {VentilatorState.used == true && <View>
                                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: "#707070" }}>At</Text>
                                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: "#707070" }}>Round {VentilatorState.Round}</Text>
                                    <Text style={{ fontSize: 30, fontWeight: 'bold', color: "#707070" }}>{moment(date).format('HH:MM:SS')} </Text>
                                    <Text style={{ fontSize: 45, fontWeight: 'bold', color: COLORS.On }}>On</Text>
                                </View>}
                            </View>
                        </View>
                        <View style={{ top: '25%', marginHorizontal: '-14%', width: scale(30) }}>
                            {VentilatorState.used == false && <Button
                                title={'On'}
                                onPress={() => {
                                    setVentilator({
                                        Round: isRound,
                                        Time: now,
                                        used: true
                                    })
                                    ActivityLog('Ventilator', 'On', now);
                                }}
                                color={'#52C41A'}
                            />}
                        </View>
                    </View>

                    <View style={{ top: '-16%' }}>
                        <View style={{ marginVertical: '2%', flexDirection: 'row' }}>
                            <View style={{ marginVertical: '2%' }}>
                                <Text style={styles.HeaderText}>EKG</Text>
                            </View>
                            <FlatList
                                data={EKG}
                                renderItem={_renderEKG}
                                keyExtractor={(item, index) => index.toString()}
                                numColumns={4}
                                ListEmptyComponent={() => {
                                    return (
                                        <View></View>
                                    )
                                }}
                            />
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.HeaderText}>Defibrillation</Text>
                                <TextInput
                                    placeholder="Please fill in Defibrillation here!"
                                    onChangeText={(defib) => setDescriptDe(defib)}
                                    keyboardType="numeric"
                                    style={{
                                        height: verticalScale(30),
                                        width: '70%',
                                        borderRadius: moderateScale(10),
                                        backgroundColor: COLORS.white,
                                        fontSize: 30,
                                        marginHorizontal: '2%',
                                    }} />


                            </View>
                        </View>

                        <View style={{ marginVertical: '1%' }}>
                            <Text style={styles.HeaderText}>Medicine</Text>
                            <View style={{ flexDirection: 'row' }}>

                                <View style={styles.Adrenaline}>
                                    <Text style={{ fontSize: SIZES.h1, fontWeight: '700', paddingTop: '3%' }}>Adrenaline</Text>
                                    
                                    <CountDown
                                        until={60*4}
                                        size={30}
                                        digitStyle={{ backgroundColor: COLORS.transparent }}
                                        digitTxtStyle={{ color: COLORS.white }}
                                        separatorStyle={{color: COLORS.white}}
                                        timeToShow={['M', 'S']}
                                        timeLabels={{m: null, s: null}}
                                        showSeparator
                                        running = {true}
                                    />
                                </View>

                                <View>
                                    <FlatList
                                        data={Medicine}
                                        renderItem={_renderMedicine}
                                        keyExtractor={(item, index) => index.toString()}
                                        numColumns={3}
                                        ListEmptyComponent={() => {
                                            return (
                                                <View></View>
                                            )
                                        }}
                                    />
                                </View>
                            </View>
                        </View>


                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                            <Text style={{ fontSize: 35, fontWeight: 'bold' }}>Activity Log</Text>


                            <View style={{ width: '100%', height: verticalScale(100) }}>
                                <Table borderStyle={{ borderWidth: 1, borderColor: '#000000' }}>
                                    <Row data={tableHead} style={styles.head} textStyle={styles.text} />
                                </Table>
                                <ScrollView style={styles.dataWrapper}>
                                    <Table borderStyle={{ borderWidth: 1, borderColor: '#000000' }}>
                                        <Rows data={ActLog} textStyle={styles.text} style={styles.head} />
                                    </Table>
                                </ScrollView>
                            </View>

                        </View>




                        <View style={{ flexDirection: 'row', marginTop: '2%', alignContent: 'flex-end' }}>
                            <TouchableWithoutFeedback disabled={Finish == true ? false : true} onPress={() => submitCPROrder('ROSC')}>
                                <View style={[styles.cardsDefib, { backgroundColor: '#62A581' }]}>
                                    <Text style={{ textAlign: 'center', fontSize: 30, color: '#FFFFFF', fontWeight: 'bold' }}>ROSC</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback disabled={Finish == true ? false : true} onPress={() => submitCPROrder('Dead')}>
                                <View style={[styles.cardsDefib, { backgroundColor: '#000000' }]}>
                                    <Text style={{ textAlign: 'center', fontSize: 30, color: '#FFFFFF', fontWeight: 'bold' }}>Dead</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback disabled={Finish == true ? false : true} onPress={() => submitCPROrder('NEXT')}>
                                <View style={[styles.cardsDefib, { backgroundColor: Finish == true ? '#FFCF7D' : '#FFFFFF' }]}>
                                    <Text style={{ textAlign: 'center', fontSize: 30, color: '#FFFFFF', fontWeight: 'bold' }}>Next Round</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    background: {
        backgroundColor: COLORS.BG
    },
    cards: {
        borderRadius: moderateScale(10),
        marginHorizontal: '2%',
        height: verticalScale(35),
        width: '20%',
        justifyContent: 'center',
        marginVertical: '2%'
    },
    cardsDefib: {
        borderRadius: moderateScale(10),
        marginLeft: '3%',
        height: '37%',
        width: '30%',
        justifyContent: 'center'
    },
    medicineModal: (item) => ({
        height: verticalScale(45),
        width: '26%',
        borderRadius: moderateScale(10),
        marginHorizontal: '1%',
        marginVertical: '1%'
    }),
    GetColorByStatus: (status) => ({
        backgroundColor:
            status == 'NEXT' ? "#F6D55C" :
                status == 'DEAD' ? "black" :
                    "#95C798"
    }),
    HeaderText: {
        fontSize: 36,
        fontWeight: 'bold',
        color: COLORS.Header
    },
    ActCard: {
        backgroundColor: '#FFFFFF',
        borderColor: '#000000',
        height: verticalScale(100),
        width: '90%',
        borderWidth: moderateScale(1)
    },
    head: {
        height: verticalScale(20),
        backgroundColor: '#FFFFFF',
    },
    text: {
        margin: moderateScale(6),
        color: "#000000",
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20
    },
    TextRe: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    dataWrapper: {
        marginTop: moderateScale(-1)
    },
    Adrenaline: {
        height: verticalScale(100),
        width: "28%",
        backgroundColor: '#4C73B8',
        borderRadius: moderateScale(10),
        marginVertical: '1%',
        alignItems: 'center'
    }
})
export default CPR;