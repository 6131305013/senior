import React, { useState, useEffect } from 'react';
import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    Modal as RNModal,
    Button,
} from 'react-native';
import PropTypes from 'prop-types';
import { moderateScale } from '../configs/scale'
import { Res } from '../configs';
import { Text } from './Text'
import moment from 'moment';
import { FlatList } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native';
import { ScrollView } from 'react-native';
import LottieView from 'lottie-react-native';

function Quiz({
    Data,
    onFinish
}) {
    const [storage, setStorage] = useState(Data)
    const [isFinish, setFinish] = useState(false)
    const [isQues, setIsQues] = useState({
        index: 0,
        Question: "",
        Choice: [],
        Correct: ""
    })
    const [isCorrect, setCorrect] = useState(0);
    useEffect(() => {
        console.log('DATA ', Data)
        setStorage(Data);
        setTimeout(() => {
            console.log('storage', storage)
            SetQuestion(0)
        }, 1000)
    }, [])

    const CheckQuiz = (choice) => {
        console.log('length : ', storage.length)
        console.log('indedx : ', isQues.index)
        if (isQues.index < (storage.length - 1)) {
            if (choice === isQues.Correct) {
                console.log('CORRECT GET 1 POINT')
                setCorrect(isCorrect + 1)
            } else {
                console.log('WRONG ANSWER')
            }
            SetQuestion(isQues.index + 1)
        } else {
            if (choice === isQues.Correct) {
                console.log('CORRECT GET 1 POINT')
                setCorrect(isCorrect + 1)
            } else {
                console.log('WRONG ANSWER')
            }
            setFinish(true)
            onFinish()
            console.log('FINISH CORRECT : ', isCorrect)
        }
    }

    const SetQuestion = (index) => {

        if (storage.length > 0 && index < storage.length) {
            let arrays = {
                index: index,
                Question: storage[index].Question,
                Choice: storage[index].Choice,
                Correct: storage[index].Correct
            }
            setIsQues(arrays)
        }
    }

    const _renderChioce = ({ item }) => {
        // console.log('item : ', item)
        return (
            <TouchableWithoutFeedback onPress={() => CheckQuiz(item)}>
                <View style={styles.Choice}>
                    <Text style={[styles.TextChoice, { color: 'white', textAlign: 'center' }]}>{item}</Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    const _renderQuiz = () => {
        return (
            <ScrollView>
                <View style={{ backgroundColor: 'transparent',alignItems:'center' }}>
                    <View style={styles.Question} >
                        <Text style={styles.TextQuest}>{isQues.Question}</Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <FlatList
                            data={isQues.Choice}
                            renderItem={_renderChioce}
                        />
                    </View>
                    <View>

                    </View>
                </View>
            </ScrollView>
        )
    }

    const _renderSummary = () => {
        return (
            <View style={{justifyContent:'center',alignItems:'center'}}>
                <LottieView source={Res.Animation.WelDone} autoPlay loop={true} style={[{ width: 250, height: 250 }]} />
            <View style={styles.Summary}>
                <View style={styles.score}>
                    <Text style={{ fontSize: 25, fontWeight: "bold", color: "#205072" }}>YOUR SCORE</Text>
                </View>
                <View>
                    <Text style={styles.TextPoint}>{isCorrect}</Text>
                    <View style={styles.divider}></View>
                    <Text style={styles.TextTotal}>{storage.length}</Text>
                </View>
                <View>

                </View>
            </View>
            </View>
        )
    }

    return (
        <View style={styles.Container}>
            {isFinish ? <_renderSummary /> : <_renderQuiz />}
        </View>
    )
}
const styles = StyleSheet.create({
    Container: {
        // flex:1,
        backgroundColor: 'transparent',
    },
    Question: {
        width: "95%",
        height: 400,
        backgroundColor: 'white',
        marginVertical: '3%',
        justifyContent: 'center',
        borderRadius: 30,
        alignItems: 'center',
    },
    Choice: {
        width: 700,
        height: 85,
        backgroundColor: '#47CACC',
        borderRadius: 10,
        marginHorizontal: '2%',
        marginVertical: '2%',
        justifyContent: 'center'
    },
    Summary: {
        width: 450,
        height: 500,
        backgroundColor: '#FFFFFF',
        justifyContent:'center',
        borderRadius: 15,
        elevation: 7
    },
    TextQuest: {
        fontSize: 27
    },
    TextChoice: {
        fontSize: 28
    },
    score: {
        alignItems: 'center',
        marginVertical: "8%"
    },
    divider: {
        borderWidth: 1,
        width: "50%",
        alignSelf: 'center',
        borderColor: '#000000'
    },
    TextPoint: {
        fontSize: 50,
        fontWeight: '700',
        textAlign: 'center',
        marginVertical: '7%',
        color: "#205072"
    },
    TextTotal: {
        fontSize: 50,
        fontWeight: '700',
        textAlign: 'center',
        marginVertical: '7%',
        color: "#205072"
    }
})
Quiz.defaultProps = {
    Data: [],
    onFinish: () => { }
}
Quiz.PropTypes = {
    Data: PropTypes.array,
    onFinish: PropTypes.func
}
export { Quiz }