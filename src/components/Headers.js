import React, { useState, useEffect, useContext, useCallback } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TouchableNativeFeedback,
  StyleSheet,
} from 'react-native';
import { Header } from 'react-native-elements';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import mockup from '../configs/mockup';
const CustomHeaders = ({
  backgroundColor,
  textColor,
  navigation,
  route,
}) => {
  useEffect(() => {
    // console.log('R E N D E R => Header (useEffect)');
    console.log('ROUTE NAME : ', route);
  }, [route]);
  const _renderLeft = () => {
    return (
      <View style={{ width: 500, height: 150 }}>
        <View style={{ alignContent: 'center', marginTop: 70, marginBottom: 78 }}>
          {/* <TouchableOpacity onPress={() => { navigation.goBack() }}>
            <MaterialIcons name="arrow-back" size={40} color={"#000000"} style={{ marginBottom: 30 }} />
          </TouchableOpacity> */}
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: 30, fontWeight: 'bold', color: '#205072' }}>{mockup.PROFILE.fristname}, </Text>
            <Text style={{ fontSize: 30, color: '#205072' }}>{mockup.PROFILE.Lastname}</Text>
          </View>

          <Text style={{ fontSize: 15, color: '#205072' }}>Welcome to CPR application</Text>
        </View>
      </View>
    )
  }

  const _renderRight = () => {
    return (
      <View>
        <Image
          source={require('../asset/Image/mockup_Boss.jpg')}
          style={{ width: 105, height: 105, borderRadius: 100, marginTop: 48, marginBottom: 32 }}
        />
      </View>
    )
  }

  return (
    <>
      <Header
        statusBarProps={{
          translucent: true,
          backgroundColor: 'white',
        }}
        barStyle='light-content'
        leftComponent={_renderLeft()}
        rightComponent={_renderRight()}
        containerStyle={[{
          backgroundColor: 'white',
          justifyContent: 'space-around',
          paddingHorizontal: 50,
          borderBottomColor: 'transparent',
          height: 200
        }, styles.headerShadows]}
      />
    </>
  )

}

const styles = StyleSheet.create({
  headerShadows: {
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.08,
    shadowRadius: 3.0,
    elevation: 15,
  },
});

const Headers = React.memo(CustomHeaders);
export { Headers };
