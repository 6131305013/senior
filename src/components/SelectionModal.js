import React, { useState, useEffect, useContext, useCallback } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    TouchableNativeFeedback,
    StyleSheet,
    FlatList
} from 'react-native';
import PropTypes from 'prop-types';
import { Header } from 'react-native-elements';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { scale, verticalScale, moderateScale } from '../configs/scale';
import { COLORS } from '../themes';

function SelectionModal({
    title,
    titleModalColor,
    titleTextColor,
    titlePosition,
    titleImage,
    choice
}) {



    const renderChoice = ({ item }) => {
        return (
            <TouchableNativeFeedback
                onPress={item.onPress}
            >
                <View>

                    <View style={styles.ChoiceModal(item.color, titlePosition)}>
                        <Text style={styles.Text(item.color, 40)}>{item.name}</Text>
                    </View>

                </View>
            </TouchableNativeFeedback>
        )
    }
    return (
        <View style={styles.container}>
            <View style={styles.BackField}>
                <View style={{ flexDirection: 'row' }}>
                    {titlePosition == 'right' && <View>
                        <FlatList
                            data={choice}
                            renderItem={renderChoice}
                        />
                    </View>}
                    <View style={styles.TitleModal(titleModalColor)}>
                        <Text style={styles.Text(titleTextColor, 60)}>{title}</Text>
                        <Image
                            style={styles.img}
                            source={titleImage}
                        />
                    </View>
                    {titlePosition == 'left' && <View>
                        <FlatList
                            data={choice}
                            renderItem={renderChoice}
                        />
                    </View>}
                </View>

            </View>
        </View>

    )
}

SelectionModal.defaultProps = {
    title: 'ModalName',
    choice: [],
    titleTextColor: 'white',
    titleModalColor: 'black',
    titlePosition: 'left',
    titleImage: null
};

SelectionModal.PropTypes = {
    name: PropTypes.string,
    choice: PropTypes.array,
    titleTextColor: PropTypes.string,
    titleModalColor: PropTypes.string,
    titlePosition: PropTypes.string,
    titleImage: PropTypes.string
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.BG
    },
    BackField: {
        marginHorizontal: moderateScale(40),
        marginVertical: moderateScale(35),
        alignItems: "center",
    },
    TitleModal: (color) => ({
        backgroundColor: color,
        width: scale(150),
        height: verticalScale(250),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: moderateScale(20),
        elevation: 15
    }),
    Text: (Color, size) => ({
        color: Color,
        fontSize: size,
        fontWeight: "bold"
    }),
    ChoiceModal: (color, position) => ({
        backgroundColor: 'white',
        width: scale(130),
        height: verticalScale(60),
        marginVertical: moderateScale(10),
        alignItems: 'center',
        justifyContent: 'center',
        borderTopRightRadius: moderateScale(position == 'left' ? 30 : 0),
        borderBottomRightRadius: moderateScale(position == 'left' ? 30 : 0),
        borderTopLeftRadius: moderateScale(position == 'right' ? 30 : 0),
        borderBottomLeftRadius: moderateScale(position == 'right' ? 30 : 0),
        shadowColor: 'rgba(0,255,0,0.5)',
        shadowOffset: { width: 10, height: 10 },
        shadowOpacity: 0.2,
        elevation: 5,
    }),
    img: {
        width: '50%',
        height: '50%',
    },
})
export { SelectionModal };