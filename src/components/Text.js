
import React from 'react';
import { Text as RNText } from 'react-native';
import PropTypes from 'prop-types';
import { Res } from '../configs';
function Text({style, size, color, regular, bold, children, ...rest}) {
    return (
        <RNText 
            style={[
                size && { fontSize: size },
                color && {color},
                regular && {fontFamily: Res.Fonts.Regular},
                bold && (Platform.OS === 'ios' ? {fontFamily: Res.Fonts.Regular, fontWeight: '600'} : {fontFamily: Res.Fonts.Bold}),
                style && style,
            ]} {...rest}>
            {children}
        </RNText>
    );
}

Text.defaultProps = {
    children: null,
    style: null,
    color: null,
    size: 0,
}

Text.propTypes = {
    children: PropTypes.any,
    style: PropTypes.any,
    color: PropTypes.string,
    size: PropTypes.number,
}

export { Text };