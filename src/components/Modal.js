import React from 'react';
import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    Modal as RNModal,
} from 'react-native';
import PropTypes from 'prop-types';
import { moderateScale } from '../configs/scale'
import { Text } from './Text'
import moment from 'moment';
import AnimatedCircularProgress from 'react-native-animated-circular-progress';
import {progressColor,progressPercent} from '../functions'
function Modal({
    score,
    Type,
    TypePerson,
    Date,
    animationType,
    modalVisible,
    onClose
}) {

    return (
        <RNModal
            animationType={animationType}
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => onClose()}
        >
            <View style={[styles.centeredView]} >
                <View style={[styles.modalStyle]} >
                    <View style={styles.headerBack}>
                        <TouchableOpacity onPress={() => onClose()}>
                        <Text>Back</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.ModalCenter]}>
                        <AnimatedCircularProgress
                            backgroundColor='#eeeeee'
                            innerBackgroundColor='white'
                            color={progressColor(score)}
                            startDeg={0}
                            endDeg={progressPercent(score)}
                            radius={110}
                            innerRadius={80}
                            duration={1000}
                            children={(
                                <View style={{alignItems:'center',marginTop:50}}>
                                    <Text style={{fontSize:50}}>{score} %</Text>
                                </View>
                            )}
                        />

                    </View>
                    <View style={[styles.ModalEnd]}>
                        <View>
                            <Text style={{ fontSize: 40 }} >{Type} ( {TypePerson} )</Text>
                            <Text style={{ fontSize: 25 }} >Date :  {moment(Date).format('DD MMM YYYY')} </Text>
                        </View>
                        <View>
                            {/* image type */}
                        </View>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                        <Text>Again</Text>
                    </View>
                </View>
            </View>
        </RNModal>
    );
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 10,
        // backgroundColor: '#00000044'
    },
    ModalCenter:{
        // flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
    },
    ModalEnd:{
        marginTop:15
    },
    modalStyle: {
        borderRadius: 7,
        borderWidth: 0,
        // alignItems: 'center',
        backgroundColor: 'white',
        paddingHorizontal: moderateScale(30),
        paddingBottom:moderateScale(15),
        paddingTop:moderateScale(15),
        elevation: 10,
        // marginVertical:moderateScale(15),
        
        
    },
    headerBack:{
        justifyContent:'flex-start'
    }

})

Modal.defaultProps = {
    modalVisible: true,
    score: 10,
    Type: 'Practice',
    TypePerson: 'Adult',
    Date: new Date(),
    animationType: 'none',
    onCancel: () => { },
};

Modal.PropTypes = {
    modalVisible: PropTypes.bool,
    score: PropTypes.number,
    Type: PropTypes.string,
    TypePerson: PropTypes.string,
    Date: PropTypes.Date,
    animationType: PropTypes.string,
    onClose: PropTypes.func,
};

export { Modal };
