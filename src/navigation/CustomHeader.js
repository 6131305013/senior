import React, { memo } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { COLORS } from '../themes';


function CustomHeader({
  title = '',
  navigation,
  left = false,
  right = false,
  onHandleRightClick = () => { },
  onHandleLiftClick = () => { }
}) {

  const _handleGoBack = () => {
    navigation.goBack();
  };

  return (
    <View>
      <View style={styles.container}>
        <Text style={styles.centerTitle}>{title}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 90,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    backgroundColor: COLORS.white
  },
  centerTitle: {
    fontSize: 40,
    color: COLORS.secondary,
    fontWeight: '700',
    fontFamily: 'SukhumvitSet-Text',
    elevation:15
  }
})
export default memo(CustomHeader);