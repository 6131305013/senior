import * as React from 'react';
import { NavigationContainer, getFocusedRouteNameFromRoute } from '@react-navigation/native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { Host } from 'react-native-portalize';
import { HomeScreen, Screen2 } from '../screen/MainStack';
import { MainCPR, CPR, ListOfPatient, summary, Calculators } from '../screen/CPRStack';
import { Learning } from '../screen/LearningStack';
import { Practice } from '../screen/PracticeStack';
import SelectLearnOrPractice from '../screen/SelectLearnOrPractice';
// import { Profile } from '../screen/ProfileStack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AntDesign from "react-native-vector-icons/AntDesign";
import { AnimatedTabBarNavigator } from "react-native-animated-nav-tab-bar";
// import { Headers } from '../components'

const Stack = createStackNavigator();
const Tab = AnimatedTabBarNavigator();

export default function Navigation({ navigate }) {
    const createMainStack = () => {
        return (
            <Stack.Navigator
                screenOptions={({ route }) => ({
                    gestureEnabled: true,
                    gestureDirection: 'horizontal',
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                    // header: ({ scene, previous, navigation, ...props }) => {
                    //     return <Headers route={route.name} navigation={navigation} />;
                    // },
                })}>
                <Stack.Screen name='HomeScreen' component={HomeScreen} options={{ headerShown: false }} />
                <Stack.Screen name='Screen2' component={Screen2} options={{ headerShown: false }} />
            </Stack.Navigator>
        )
    };



    const createCPRStack = () => {
        return (
            <Stack.Navigator
                screenOptions={({ route }) => ({
                    gestureEnabled: true,
                    gestureDirection: 'horizontal',
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                    // header: ({ scene, previous, navigation, ...props }) => {
                    //     return <Headers route={route.name} navigation={navigation} />;
                    // },
                })}>
                <Stack.Screen name="ListOfPatient" component={ListOfPatient} options={{ headerShown: false }} />
                <Stack.Screen name="MainCPR" component={MainCPR} options={{ headerShown: false }} />
                <Stack.Screen name="CPR" component={CPR} options={{ headerShown: false }} />
                <Stack.Screen name="summary" component={summary} options={{ headerShown: false }} />
                <Stack.Screen name="Calculators" component={Calculators} options={{ headerShown: false }} />
            </Stack.Navigator>
        );
    };

    const createLearningAndPracticeStack = () => {
        return (
            <Stack.Navigator
                initialRouteName="Selection"
                screenOptions={({ route }) => ({
                    gestureEnabled: true,
                    gestureDirection: 'horizontal',
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                    // header: ({ scene, previous, navigation, ...props }) => {
                    //     return <Headers route={route.name} navigation={navigation} />;
                    // },
                })}>
                <Stack.Screen name="Selection" component={SelectLearnOrPractice} options={{ headerShown: false }} />
                <Stack.Screen name="Practice" component={Practice} options={{ headerShown: false }} />
                <Stack.Screen name="Learning" component={Learning} options={{ headerShown: false }} />
            </Stack.Navigator>
        );
    };

    // const createProfileStack = () => {
    //     return (
    //         <Stack.Navigator
    //             screenOptions={({ route }) => ({
    //                 gestureEnabled: true,
    //                 gestureDirection: 'horizontal',
    //                 cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    //                 // header: ({ scene, previous, navigation, ...props }) => {
    //                 //     return <Headers route={route.name} navigation={navigation} />;
    //                 // },
    //             })}>
    //             <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
    //         </Stack.Navigator>
    //     );
    // };


    const BottomTabStack = ({ navigation }) => {


        return (
            <Tab.Navigator
                tabBarOptions={{
                    activeTintColor: "white",
                    activeBackgroundColor: '#62A581'
                }}
            >
                <Tab.Screen
                    name="Home"
                    component={createMainStack}
                    options={{
                        tabBarIcon: ({ tintColor }) => (
                            <MaterialIcons name="home" size={23} color={"#000000"} />
                        )
                    }}
                />
                <Tab.Screen
                    name="CPR"
                    component={createCPRStack}
                    options={{
                        tabBarIcon: ({ tintColor }) => (
                            <AntDesign name="hearto" size={23} color={"#000000"} />
                        ),
                        tabBarVisible: () => {
                            let routes = navigation.state;
                            let tabBar;
                            if (routes == "CPR") {
                                tabBar = false;
                            } else {
                                tabBar = true;
                            }
                            return tabBar
                        }
                    }}
                />
                <Tab.Screen
                    name="Learning&Practice"
                    component={createLearningAndPracticeStack}
                    options={{
                        tabBarIcon: ({ tintColor }) => (
                            <AntDesign name="book" size={23} color={"#000000"} />
                        )
                    }}
                />
                {/* <Tab.Screen
                    name="Profile"
                    component={createProfileStack}
                    options={{
                        tabBarIcon: ({ tintColor }) => (
                            <MaterialIcons name="person" size={23} color={"#000000"} />
                        )
                    }}
                /> */}

            </Tab.Navigator>
        )
    };


    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{ headerShown: false }}
            >
                <Stack.Screen name='MainStack' component={BottomTabStack} />
                {/* <Stack.Screen name="HomeScreen" component={BottomTabStack} /> */}
            </Stack.Navigator>
        </NavigationContainer>
    )
}