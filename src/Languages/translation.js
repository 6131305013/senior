import AsyncStorage from '@react-native-community/async-storage'; 
import React, {createContext, useState} from 'react';
import LocalizedStrings from 'react-native-localization'; 
import {DEFAULT_LANGUAGE,APP_LANGUAGE} from '../configs';
import en from './en.json'
import th from './th.json'

const languages = {th, en};
const translations = new LocalizedStrings(languages); 

export const LocalizationContext = createContext({
    // 5
    translations,
    setAppLanguage: () => {}, 
    appLanguage: DEFAULT_LANGUAGE, 
    initializeAppLanguage: () => {}, 
  
  });

  export const LocalizationProvider = ({children}) => {

    const [appLanguage, setAppLanguage] = useState(DEFAULT_LANGUAGE);

    const setLanguage = (language) => {
      translations.setLanguage(language);
      setAppLanguage(language);
      AsyncStorage.setItem(APP_LANGUAGE, language);
    };

    const initializeAppLanguage = async () => {
      const currentLanguage = await AsyncStorage.getItem(APP_LANGUAGE);
  
      if (currentLanguage) {
        console.log('Current Language ==> ', currentLanguage);
        setLanguage(currentLanguage);
      } else {
        let localeCode = DEFAULT_LANGUAGE;
        setLanguage(localeCode);
      }
    };
    
    return (
      <LocalizationContext.Provider
        value={{
          translations,
          setAppLanguage: setLanguage, // 10
          appLanguage,
          initializeAppLanguage,
        }}>
        {children}
      </LocalizationContext.Provider>
    );
  };