import AsyncStorage from '@react-native-community/async-storage';
import createDataContext from './createDataContext';
import { API } from '../configs';
import { Alert } from 'react-native';

var jwtDecode = require('jwt-decode');

const initialState = {
    loggedIn: false,
    user: {},
    userData: {},
    tabbarVisible: true,
  };

  const AuthReducer = (state, action) => {
    switch (action.type) {
      // FOR USER
      case "GET_AUTHEN":
        return { ...state, loggedIn: true, user: action.payload }
      case 'SIGN_OUT':
        return { ...state, loggedIn: false, user: {}, userData: {},}
      case 'GET_DATA':
        return { ...state, userData: action.payload }
  
      // FOR TABBAR
      case 'OPEN_TAB_BUTTON':
        return {...state, tabbarVisible: true};
      case 'CLOSE_TAB_BUTTON':
        return {...state, tabbarVisible: false};
      default:
        return state;
    }
  }

  const getAuth = (dispatch) => {
      return async (callback) => {
          const userToken = await AsyncStorage.getItem(API.AS_USER_TOKEN);
          const userData = await AsyncStorage.getItem( API.AS_USER_DATA );
          var expireDate = parseInt(await AsyncStorage.getItem( API.AS_USER_EXPIRY ));
          var currentTime = Date.now();
          if (userToken) {
            console.log('[ ⏰ ] Current Time : ', currentTime);
            console.log('[ ⏰ ] Expires Time : ', expireDate);

            const responseJson = JSON.parse(userToken);
      const responseDecode = jwtDecode(responseJson.access_token);
      console.log('[ ✅ ] Decode JWT : ', responseDecode.email);
          
      if (currentTime <= expireDate) {
        console.log('[ 👍 ] TOKEN_NOT_EXPIRE (No refresh token)')
        await new Promise((resolve, reject) => {
            resolve()
            dispatch({ type: 'GET_AUTHEN', payload: responseDecode })
          }).then( async () => {})
          
          return Promise.resolve(true);
    }else{
        console.log('[ 👎 ] TOKEN_EXPIRE (Need to refresh token)')  
    }
    }else {
        console.log('[ ❌ ] NO_AUTH_IN_ASYNCSTORAGE')
      } 
        }
  }

  const getData = (dispatch) => {
    return async () => {
      const userData = await AsyncStorage.getItem( API.AS_USER_DATA );
      const responseJson = JSON.parse(userData);
      if (userData) {
        console.log('[ ✅ ] SET_USER_DATA', userData)
        dispatch({ type: 'GET_DATA', payload: responseJson})
      } else {
        console.log('[ ❌ ] NO_AUTH_DATA_IN_ASYNCSTORAGE')
      }
    }
  }

  const SignOut = (dispatch) => {

    return async () => {
      try {
        console.log('LOGOUT')
        await AsyncStorage.removeItem( API.AS_USER_TOKEN );
        await AsyncStorage.removeItem( API.AS_USER_EXPIRY );
        await AsyncStorage.removeItem( API.AS_USER_DATA );
        dispatch({ type: 'SIGN_OUT' })
        return RootNavigation.navigate('SearchScreen');
      } catch {
        Alert.alert('Error Sign Out')
      }
    }
  }

  const TabButtonToggle = (dispatch) => {
    return async (value) => {
      console.log('Click Toggle Button ==> ', value)
      if (value) {
          console.log('Check True')
          return dispatch({type: 'OPEN_TAB_BUTTON'});
      } else {
          console.log('Check False')
          return dispatch({type: 'CLOSE_TAB_BUTTON'});
      }
    };
  };

  export const { Context, Provider } = createDataContext(  
      AuthReducer,
    { getAuth, SignOut, getData, TabButtonToggle },
    initialState
    );