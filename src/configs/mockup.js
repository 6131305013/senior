export default {
    DATA: [
        {
            id: '1',
            title: 'ปั๊มหัวใจ',
            subtitle: "ก่อนการปั๊มหัวใจควรสำรวจสิ่งต่าง ๆ รอบตัวเสียก่อน ",
            color: "#E3A39A",
            require: '..\images\heart-rate.png'
        },
        {
            id: '2',
            title: 'ผายปอด',
            subtitle: "การช่วยหายใจ เป็นวิธีที่จะช่วยให้ออกซิเจนเข้าสู่ปอดผู้ป่วยได้",
            color: "#DDD8F0"
        },
        {
            id: '3',
            title: 'AED',
            subtitle: "เป็นอุปกรณ์ที่สามารถวินิจฉัยภาวะหัวใจเต้นผิดจังหวะที่เป็นอันตรายถึงชีวิตได้โดยอัตโนมัติ",
            color: "#D8F0E3"
        },
    ],
    STAT: [
        {
            id: '1',
            title: "Practice",
            score: 100,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '2',
            title: "Practice",
            score: 90,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '3',
            title: "Practice",
            score: 80,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '4',
            title: "Practice",
            score: 70,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '5',
            title: "Practice",
            score: 60,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '6',
            title: "Practice",
            score: 50,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '7',
            title: "Practice",
            score: 40,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '8',
            title: "Practice",
            score: 30,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '9',
            title: "Practice",
            score: 20,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '10',
            title: "Practice",
            score: 10,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '11',
            title: "Practice",
            score: 70,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '12',
            title: "Practice",
            score: 70,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '13',
            title: "Practice",
            score: 70,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '14',
            title: "Practice",
            score: 70,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
        {
            id: '15',
            title: "Practice",
            score: 70,
            success: true,
            typePerson: 'children',
            type: 'Practice',
            Date: new Date()
        },
    ],
    STATLearn: [
        {
            id: '1',
            title: "Learning",
            score: 100,
            success: true,
            typePerson: 'Adult',
            type: 'Learning',
            Date: new Date()
        },
        {
            id: '2',
            title: "Learning",
            score: 100,
            success: true,
            typePerson: 'children',
            type: 'Learning',
            Date: new Date()
        },
        {
            id: '3',
            title: "Learning",
            score: 0,
            success: false,
            typePerson: 'Adulr',
            type: 'Learning',
            Date: new Date()
        },
        {
            id: '4',
            title: "Learning",
            score: 100,
            success: true,
            typePerson: 'children',
            type: 'Learning',
            Date: new Date()
        }
    ],
    PROFILE: {
        fristname: 'JAKKATAN',
        Lastname: 'KATANA',
        photo: '../asset/Image/mockup_Boss.jpg'
    },
    HistoryCPR: [
        {
            id: 1,
            Time: '12:02 PM',
            EKG: 'VF',
            Medicine: 'Adrenarine',
            Defib: true
        },
        {
            id: 2,
            Time: '12:06 PM',
            EKG: 'VT',
            Medicine: 'Amiodarone',
            Defib: true
        },
        {
            id: 1,
            Time: '12:08 PM',
            EKG: 'VF',
            Medicine: 'Amiodarone',
            Defib: false
        },
    ],
    ListPatient: [
        {
            fristname: 'Thaksa',
            lastname: 'Nanan',
            age: '18',
            status: 'Safety'
        },
        {
            fristname: 'Jakkatan',
            lastname: 'Kanata',
            age: '19',
            status: 'Safety'
        },
        {
            fristname: 'Thomas',
            lastname: 'Lee',
            age: '18',
            status: 'Safety'
        },
        {
            fristname: 'Suttida',
            lastname: 'Chansama',
            age: '18',
            status: 'Safety'
        },
    ],
    user: {
        id: 10,
        firstname: "thaksa",
        lastname: "nanan",
        createdAt: "1618277831",
        email: "test3@hotmail.com",
        username: "ThaksaNanan2",
        password: "test1234"
    }

}