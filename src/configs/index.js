import theme, { COLORS, SIZES, FONTS } from '../themes'
export const DEFAULT_LANGUAGE = 'th';
export const APP_LANGUAGE = 'appLanguage';

export const FormatYearToDay = 'YYYY-MM-DD';
export const FormatDayToYear = 'DD/MM/YYYY';
export const FormatDayAndMonth = 'DD/MM';
export const FormatMonth = 'MMM YYYY';

export const now = new Date();

const MFU_URL = 'http://selab.mfu.ac.th:8311'
const LOCAL_URL = 'http://localhost:3000'
const HEROKU_URL = 'https://secpr.herokuapp.com'

const PATH = {
  ALL_CASE: '/api/case/all',
  CASE_BY_ID: '/api/case/',
  ALL_USER: '/api/user/all',
  ALL_CPR: '/api/cpr/all',
  CPR_BY_CASE: '/api/cpr/byCase/',
  MEDICINE: '/api/medicine/',
  SIGN_IN: '/api/user/login',
  SIGN_UP: '/api/user/register',
  NEW_CASE: '/api/case/newCase',
  DEL_CPR: '/api/cpr/deleteCPR/',
  NEW_CPR: '/api/cpr/createCPR',
  UP_CASE: '/api/case/updateCase/',
  DEL_CASE: '/api/case/deleteCase/',
  UP_CPR: '/api/cpr/updateCPR/',
  CASE_BY_UID : '/api/case/byUserId/',
  GET_LESSON_COMPLETE : '/api/CompleteLearning/getLesson/',
  GET_PRACTICE_COMPLETE :'/api/CompletePractice/',
  GET_PRACTICE :'/api/practice/',
  GET_LEARNING : '/api/learning/',
  CREATE_LEARNING_COMPLETE : '/api/CompleteLearning/createCompleteLearning',
  CREATE_PRACTICE_COMPLETE : '/api/CompletePractice/createCompletePractice',
  CREATE_ACTIVITY : '/api/Activity/newActivity',
  ALL_ACTIVITY : '/api/Activity/all',
  ACTIVITY_BY_CASE_UID : '/api/Activity/byUserId/',
  EXPORT_SHEETS : '/api/Activity/createSheets',
  GET_MEDICINE_BY_TYPE : '/api/medicine/' 
}

export const API = {
  GET_ALL_CASE: MFU_URL + PATH.ALL_CASE,
  GET_CASE_BY_ID: MFU_URL + PATH.CASE_BY_ID,
  GET_ALL_USER: MFU_URL + PATH.ALL_USER,
  GET_ALL_CPR: MFU_URL + PATH.ALL_CPR,
  GET_CPR_BY_CASE: MFU_URL + PATH.CPR_BY_CASE,
  GET_MEDICINE: MFU_URL + PATH.MEDICINE,
  SIGN_IN: MFU_URL + PATH.SIGN_IN,
  SIGN_UP: MFU_URL + PATH.SIGN_UP,
  CREATE_CASE: MFU_URL + PATH.NEW_CASE,
  DELETE_CASE: MFU_URL + PATH.DEL_CASE,
  CREATE_CPR: MFU_URL + PATH.NEW_CPR,
  UPDATE_CASE: MFU_URL + PATH.UP_CASE,
  DELETE_CPR: MFU_URL + PATH.DEL_CPR,
  UPDATE_CPR: MFU_URL + PATH.UP_CPR,
  GET_CASE_BY_UID : MFU_URL + PATH.CASE_BY_UID,
  GET_LESSON_COMPLETE : MFU_URL + PATH.GET_LESSON_COMPLETE,
  GET_PRACTICE_COMPLETE :MFU_URL + PATH.GET_PRACTICE_COMPLETE,
  GET_PRACTICE :MFU_URL + PATH.GET_PRACTICE,
  GET_LEARNING : MFU_URL + PATH.GET_LEARNING,
  CREATE_LEARNING_COMPLETE : MFU_URL + PATH.CREATE_LEARNING_COMPLETE,
  CREATE_PRACTICE_COMPLETE : MFU_URL + PATH.CREATE_PRACTICE_COMPLETE,
  CREATE_ACTIVITY : MFU_URL + PATH.CREATE_ACTIVITY,
  ALL_ACTIVITY : MFU_URL + PATH.ALL_ACTIVITY,
  ACTIVITY_BY_CASE_UID : MFU_URL + PATH.ACTIVITY_BY_CASE_UID,
  EXPORT_SHEETS:  MFU_URL + PATH.EXPORT_SHEETS,
  GET_MEDICINE_BY_TYPE: MFU_URL + PATH.GET_MEDICINE_BY_TYPE,


  // AsyncStorage Constant
  AS_USER_TOKEN: 'userToken',
  AS_USER_EXPIRY: 'userExpiry',
  AS_USER_DATA: 'userData',
}

export const Res = {
  Fonts: {
    Regular: 'NotoSansThai-Regular',
    Medium: 'NotoSansThai-Medium',
    SemiBold: 'NotoSansThai-SemiBold',
    Bold: 'NotoSansThai-Bold',
  },
  Sizes: {
    Huge: 30,
    Large_Title: 24,
    Title: 22,
    Header: 16,
    Body: 14,
    Subhead: 12,
    Footnote: 15,
    Caption_1: 14,
    Caption_2: 13,
    Label: 12,
    Tiny: 10,
  },
  NumberColumns: 3,
  Bt_Active: 0.95,
  breakpoints: ['40em', '52em', '64em', '80em'],
  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
  fontSizes: [12, 14, 16, 18, 24, 36, 48, 64, 72],

  // Star Rating Count
  ratingCount: 5,
  Animation:{
    SandClock : require('../asset/animation/SandClock.json'),
    countDownGO : require('../asset/animation/countDownGO.json'),
    correct : require('../asset/animation/correct.json'),
    failed : require('../asset/animation/failed.json'),
    passed : require('../asset/animation/passed.json'),
    notpassed : require('../asset/animation/notpassed.json'),
    WelDone : require('../asset/animation/14584-well-done.json')
  },
  Image:{
    Ventilator : require('../asset/Image/ventilation.png'),
    ETTube : require('../asset/Image/ETTube.png'),
    Books: require('../asset/Image/books.png'),
    practice : require('../asset/Image/practice.png')
  }
};

export { theme, COLORS, SIZES, FONTS };
