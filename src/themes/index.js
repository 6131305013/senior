import { Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window');

export const COLORS = {
  // base colors
  primary: '#B4D6C1', // green
  secondary: '#024D82', // dark green

  lightblue: '#D0E4FF',

  green: '#49A84C', 
  BG:'#F2F1F6',
  red: '#EA4335',

  black: '#000000', //#1E1F20

  white: '#FFFFFF',

  lightgrey: '#EFEFEF',
  gray: '#C4C4C4',
  darkgray: '#999999',

  transparent: 'transparent',
  //Font
  Header:'#205072',
  input:'#E4E3E9',
  // medicine
  adre: '#FF91BB',
  calc: '#FFF37D',
  gluc: '#4AC6D7',
  amio:'#F69E00',
  nahc:'#937465',
  lido:'#9C56B9',

  //LessonTitle
  HeartP:'#F1BFB9',
  MtoM:'#DDD8F0',
  AED:'#D8F0E3',
  child:'#62B3F2',
  adult:'#3FD483',

  //Button
  select:'#D2E3FC',
  list:'#CEEAD6',
  quick:'#E1432E',
  rosc:'#4E9C81',
  dead:'#000000',
  nextR:'#FFCF7D',
  ActiLog:'#A75EA4',
  On:'#34A853',
  // Bank
  SCB: '#532882',
  KBANK: '#00811A',
  KTB: '#01A6E6',
  BBL: '#223D98',

  // white To Black
  WTB: [
    '#FFFFFFCC',
    '#FFFFFFCC',
    '#FFFFFF80',
    '#FFFFFF80',
    '#00000080',
    '#000000CC',
  ],

  // Black To Transparent
  BTT: ['#000', '#00000080', '#0000001A'],

  // Blue To White
  BTW: ['#D0E4FF', '#E6F8FF'],

  // Transparent To White
  TTW: [
    'rgba(255,255,255,0)',
    'rgba(255,255,255,0.2)',
    'rgba(255,255,255,0.5)',
    'rgba(255,255,255,0.8)',
    'rgba(255,255,255,1)',
  ],

  // Transparent To Black
  TTB: ['rgba(0,0,0,0)', 'rgba(0,0,0,0.05)'],

  // Inner Shadow
  INNER_SHADOW: [
    'rgba(0,0,0,0.1)',
    'rgba(0,0,0,0)',
    'rgba(0,0,0,0)',
    'rgba(0,0,0,0)',
    'rgba(0,0,0,0.1)',
  ],
};

export const SIZES = {
  // global sizes
  base: 8,
  font: 14,
  radius: 30,
  padding: 10,
  padding2: 12,

  // font sizes
  largeTitle: 30,
  h1: 24,
  h2: 22,
  h3: 20,
  h4: 18,
  h5: 16,
  h6: 14,
  body1: 20,
  body2: 18,
  body3: 16,
  body4: 14,
  body5: 12,
  body6: 10,

  // app dimensions
  width,
  height,
};

export const FONTS = {
  largeTitle:
    Platform.OS === 'ios'
      ? {
          fontFamily: 'SukhumvitSet-Text',
          fontSize: SIZES.largeTitle,
          fontWeight: '600',
        }
      : { fontFamily: 'SukhumvitSet-Bold', fontSize: SIZES.largeTitle },

  h1:
    Platform.OS === 'ios'
      ? {
          fontFamily: 'SukhumvitSet-Text',
          fontSize: SIZES.h1,
          fontWeight: '600',
        }
      : { fontFamily: 'SukhumvitSet-Bold', fontSize: SIZES.h1 },

  h2:
    Platform.OS === 'ios'
      ? {
          fontFamily: 'SukhumvitSet-Text',
          fontSize: SIZES.h2,
          fontWeight: '600',
        }
      : { fontFamily: 'SukhumvitSet-Bold', fontSize: SIZES.h2 },

  h3:
    Platform.OS === 'ios'
      ? {
          fontFamily: 'SukhumvitSet-Text',
          fontSize: SIZES.h3,
          fontWeight: '600',
        }
      : { fontFamily: 'SukhumvitSet-Bold', fontSize: SIZES.h3 },

  h4:
    Platform.OS === 'ios'
      ? {
          fontFamily: 'SukhumvitSet-Text',
          fontSize: SIZES.h4,
          fontWeight: '600',
        }
      : { fontFamily: 'SukhumvitSet-Bold', fontSize: SIZES.h4 },

  h5:
    Platform.OS === 'ios'
      ? {
          fontFamily: 'SukhumvitSet-Text',
          fontSize: SIZES.h5,
          fontWeight: '600',
        }
      : { fontFamily: 'SukhumvitSet-Bold', fontSize: SIZES.h5 },

  body1: { fontFamily: 'SukhumvitSet-Text', fontSize: SIZES.body1 },
  body2: { fontFamily: 'SukhumvitSet-Text', fontSize: SIZES.body2 },
  body3: { fontFamily: 'SukhumvitSet-Text', fontSize: SIZES.body3 },
  body4: { fontFamily: 'SukhumvitSet-Text', fontSize: SIZES.body4 },
  body5: { fontFamily: 'SukhumvitSet-Text', fontSize: SIZES.body5 },
  body6: { fontFamily: 'SukhumvitSet-Text', fontSize: SIZES.body6 },
  // lineHeight: 0
};

const appTheme = { COLORS, SIZES, FONTS };

export default appTheme;
