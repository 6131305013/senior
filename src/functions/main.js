import handleGetCompletePractice from '../api/handleGetCompletePractice';
import handleGetCompleteLesson from '../api/handleGetCompleteLesson';

const [getCompleteLesson] = handleGetCompleteLesson();
const [getCompletePractice] = handleGetCompletePractice();

const DetailByType = async (id, isSelect) => {
    let arrays = [];
    let getData = [];
    if (isSelect == 'Practice') {
        getData = await getCompletePractice(id);
    } else if (isSelect == 'Learning') {
        getData = await getCompleteLesson(id);
    }
    let index = 0;
    for (let i in getData) {
        let object = {
            id: index + 1,
            title: isSelect,
            score: isSelect == 'Practice' ? getData[i].score : 0,
            success:
                isSelect == 'Practice' ?
                    getData[i].score > 50 ? true : false
                    :
                    isSelect == 'Learning' ? getData[i].success
                        : null
            ,
            typePerson: getData[i].lesson,
            type: isSelect,
            Date: getData[i].at
        }
        arrays.push(object);
        index++;
    }
    console.log('ARRAYS : ', arrays)
    return arrays;
}

export {DetailByType}