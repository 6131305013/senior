const progressColor = (point) => {
    if (point >= 90) {
        return 'green'
    }else if(point >= 75 && point < 90){
        return '#57E326'
    }else if(point >= 60 && point < 75){
        return '#A1E326'
    }else if(point >= 50 && point < 60){
        return '#D5E326'
    }else if(point >= 35 && point < 50){
        return '#E3BB26'
    }else if(point >= 20 && point < 35){
        return '#E37F26'
    }else if (point < 20){
        return '#F93B3B'
    }
}
const progressPercent = (point) => {
    return (360/100)*point
}

export {progressColor,progressPercent }