import Navigation from './src/navigation';
// import {LocalizationProvider} from './src/Languages/translation';
import * as React from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context';

export default function App({ navigation}) {
  return (
    <SafeAreaProvider>
    {/* // <LocalizationProvider> */}
     <Navigation/>
    {/* // </LocalizationProvider> */}
    </SafeAreaProvider>
  )
}
